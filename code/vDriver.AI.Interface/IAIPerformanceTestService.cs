﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.AI.Interface
{
    public interface IAIPerformanceTestService : IAIService
    {
        void SetExpectedMessageCount(int messageCount);
        void SetJobSimulationTime(TimeSpan time);
    }
}
