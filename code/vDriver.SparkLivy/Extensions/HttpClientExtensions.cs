﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;

using Newtonsoft.Json.Linq;
using vDriver.Library;

namespace vDriver.SparkLivy
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsync(this HttpClient client, string url, string body) =>
            client.PostAsync(url, new StringContent(body));

        //public static Task<HttpResponseMessage> PostAsync(this HttpClient client, string url, object body) =>
        //    client.PostAsync(url, body.ToJsonString());

        public static Task<HttpResponseMessage> PostAsync(this HttpClient client, string url, JObject body) =>
            client.PostAsync(url, body.ToJsonString());
    }
}
