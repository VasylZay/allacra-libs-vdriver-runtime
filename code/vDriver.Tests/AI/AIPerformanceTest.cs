﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.AmazonS3.Interface;
using vDriver.DocumentLocker.Interface;
using vDriver.RabbitMQ.Interface;
using vDriver.SparkLivy.Interface;



namespace vDriver.Tests.AI
{
    public class AIPerformanceTest
    {

        public AIPerformanceTest(IServiceProvider serviceProvider)
        {
            this.ServiceProvider = serviceProvider;
        }

        public IServiceProvider ServiceProvider { get; set; }

        public async Task<string> Run(int expectedMesageCount, TimeSpan jobTime)
        {
            string rslt = "";

            string msg = String.Format("Started processing using AIPerformanceTestService. Expected message count: {0}, work simulation: {1} ms", expectedMesageCount, jobTime.TotalMilliseconds);
            Console.WriteLine(msg);

            var mqTest = this.ServiceProvider.GetService<IRabbitMQTest>();
            await mqTest.SendMessagesForPerformanceTest(expectedMesageCount);

            await Task.Delay(expectedMesageCount * 30);

            var serviceAI = ServiceProvider.GetService<IAIPerformanceTestService>();
            serviceAI.SetExpectedMessageCount(expectedMesageCount);
            serviceAI.SetJobSimulationTime(jobTime);

            await serviceAI.Run();

            return rslt;
        }

    }
}
