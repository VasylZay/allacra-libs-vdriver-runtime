﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.SparkLivy
{
    // [VZ:] This object reprents the state of batch session. 
    // It can be obtained as a  result of batch job state request:
    // GET /batches/{batchId}/state
    // https://livy.incubator.apache.org/docs/latest/rest-api.html#batch 
    public class LivyBatch
    {
        public int id;
        public string state;
    }
}
