﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.Interface
{
    public interface ILogger
    {
        void Trace(string message);
    }
}
