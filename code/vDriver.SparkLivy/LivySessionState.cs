﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.SparkLivy
{
    public enum LivySessionState
    {
        Unknown = 0,
        Available,
        Idle,
        Dead,
        Error,
        Starting,
        Waiting,
        Running,
        Busy,
        ShuttingDown
    }
}
