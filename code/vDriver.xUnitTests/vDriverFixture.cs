﻿using System;

using vDriver.Runtime;

namespace vDriver.IntegrationTests
{
    public class vDriverFixture
    {

        public vDriverFixture()
        {
            vDriverApp app = new vDriverApp();
            app.Initialize();
            app.Start();

            this.App = app;
        }

        public vDriverApp App { get; private set; }

    }
}
