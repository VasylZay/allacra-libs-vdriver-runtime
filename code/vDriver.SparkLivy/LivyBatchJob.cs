﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json.Linq;
using Polly;

using vDriver.SparkLivy.Interface;
using vDriver.Library;

namespace vDriver.SparkLivy
{
    public class LivyBatchJob : ILivyBatchJob
    {
        private readonly ILogger _logger; 
        private readonly ISparkLivyConfig _config;

        private readonly HttpClient _client;

        public LivyBatchJob(ILogger logger, ISparkLivyConfig config)
        {
            this._logger = logger;
            this._config = config;

            this._client = LivyHttpClientFactory.CreateClient(config.ServerUrl);
        }

        public async Task<ILivyBatchJobResult> ExecuteBatchJobAsync(JObject dataObject)
        {
            LivyBatchJobResult result = new LivyBatchJobResult();

            using (var content = new StringContent(dataObject.ToJsonString(), Encoding.UTF8, "application/json"))
            {
                string batchPath = null;

                try
                {
                    HttpResponseMessage response = 
                        await this._client.PostAsync("batches", content).ConfigureAwait(false);

                    response.EnsureSuccessStatusCode();

                    batchPath = response.Headers.Location.AsRelativePath();

                    var (State, Result) = await WaitForBatchJobStateAsync(batchPath, 
                        new[] { LivyBatchJobState.Success, LivyBatchJobState.Error })
                        .ConfigureAwait(false);
                    if(State == LivyBatchJobState.Success)
                    {
                        result = new LivyBatchJobResult
                        {
                            SuccessFlag = true,
                            Result = Result,
                            Message = "Batch job finished successfully!"
                        };
                    }
                }
                catch (Exception ex)
                {
                    result = new LivyBatchJobResult
                    {
                        SuccessFlag = false,
                        Result = null,
                        Message = "Batch job failed: " + Environment.NewLine + ex.Message
                    };

                    this._logger.LogError(ex, "Spark batch job failed!");
                }
                finally
                {
                    if (batchPath != null)
                    {
                        HttpResponseMessage cleanupResponse = await this.CleanupAsync(batchPath);
                    }
                }
            }

            return result;
        }


        private async Task<(LivyBatchJobState State, JObject Result)> WaitForBatchJobStateAsync(string batchPath, params LivyBatchJobState[] expectedStates)
        {
            int pollingDelay = this._config.SparkLivyBatchJobPollingDelay;
            while(true)
            {
                LivyBatchJobState state = await this.GetBatchJobState(this._client, batchPath)
                    .ConfigureAwait(false);

                if (expectedStates.Contains(state))
                {
                    var jObject = await GetResultAsync(this._client, batchPath)
                        .ConfigureAwait(false);

                    return (state, jObject);
                }

                await Task.Delay(pollingDelay).ConfigureAwait(false);
            }
        }

          
        private async Task<LivyBatchJobState> GetBatchJobState(HttpClient client, string batchPath)
        {
            LivyBatchJobState state = LivyBatchJobState.Unknown;

            string url = client.BaseAddress.ToString() + batchPath + "/state";
            try
            {
                HttpResponseMessage response = await Policy.Handle<TaskCanceledException>()
                    .Or<HttpRequestException>()
                    .WaitAndRetryAsync(new []
                    {
                        TimeSpan.FromMilliseconds(100),
                        TimeSpan.FromMilliseconds(200),
                        TimeSpan.FromMilliseconds(500),
                        TimeSpan.FromMilliseconds(1000)
                    })
                    .ExecuteAsync(() => client.GetAsync(url))
                    .ConfigureAwait(false);

                response.EnsureSuccessStatusCode();

                LivyBatch batch = await response.Content.ReadAsAsync<LivyBatch>().ConfigureAwait(false);
                state = this.ParseJobState(batch.state);
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex, "Failed to get batch job state.");
                state = LivyBatchJobState.Error;
            }

            return state;
        }



        private static async Task<JObject> GetResultAsync(HttpClient client, string url)
        {
            HttpResponseMessage response = await Policy.Handle<TaskCanceledException>()
                .Or<HttpRequestException>()
                .RetryAsync(3)
                .ExecuteAsync(() => client.GetAsync(url))
                .ConfigureAwait(false);

            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return JObject.Parse(result);
        }

        private LivyBatchJobState ParseJobState(string stateString) =>
            Enum.TryParse<LivyBatchJobState>(stateString, true, out var state) ?
        state : LivyBatchJobState.Error;


        private async Task<HttpResponseMessage> CleanupAsync(string batchId)
        {
            return await this._client.DeleteAsync(batchId);
        }

    }
}
