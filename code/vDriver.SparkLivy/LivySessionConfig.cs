﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace vDriver.SparkLivy
{
    public class LivySessionConfig
    {
        public static LivySessionConfig Default { get; } = new LivySessionConfig
        {
            Kind = SparkCodeKind.Spark
        };

        [JsonConverter(typeof(StringEnumConverter), true)]
        public SparkCodeKind Kind { get; set; } = SparkCodeKind.Spark;
        public string Name { get; set; }
        public string ExecutorMemory { get; set; }
        public int? ExecutorCores { get; set; }
        public int? NumExecutors { get; set; }
        public int? DriverCores { get; set; }
        public IList<string> Jars { get; } = new List<string>();
        public IDictionary<string, string> Conf { get; } = new Dictionary<string, string>();
        public string Url { get; set; }
        public string ApplicationId { get; set; }

    }
}
