﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using vDriver.SparkLivy.Interface;

namespace vDriver.SparkLivy
{
    public class LivyBatchJobResult : ILivyBatchJobResult
    {
        public bool SuccessFlag { get; set; } = false;
        public string Message { get; set; } = "Batch job did not started yet";
        public JObject Result { get; set; }
    }
}
