﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Xunit;

using vDriver.Runtime;
using vDriver.RabbitMQ.Interface;

namespace vDriver.IntegrationTests
{
    public class RabbitMQ_SimpleTest : IClassFixture<vDriverFixture>
    {
        private readonly vDriverApp _app;
        private readonly IRabbitMQService _srv;

        public RabbitMQ_SimpleTest(vDriverFixture fixture)
        {
            this._app = fixture.App;
            this._srv = this._app.GetServiceProvider().GetService<IRabbitMQService>();
        }

        // [VZ:REVISIT] Dependency Injection works here erratically. Shall we have xUnit for .NET Core instead for .NET Framework?
        //public RabbitMQ_SimpleTest(vDriverFixture fixture, IRabbitMQService srv)
        //{
        //    this._app = fixture.App;
        //    this._srv = srv;
        //}

        [Fact]
        [Trait("Category", "Integration")]
        public async Task Test()
        {
            var srv = this._app.GetServiceProvider().GetService(typeof(IRabbitMQService));

            await Task.Delay(10);

            // Arrange ---------------------------------------------
            //LivySessionConfig config = new LivySessionConfig
            //{
            //    Url = vDriverConfig.GetLivySparkClusterUrl()
            //};
            // -----------------------------------------------------

            // Act -------------------------------------------------
            //LivySession session = await LivySparkSessionConnector.CreateSessionAsync(config);
            //Assert.NotNull(session);

            //bool sessionReady = await session.WaitForSessionAsync().ConfigureAwait(false);
            //session.Dispose();
            // -----------------------------------------------------

            // Assert ----------------------------------------------
            //Assert.True(sessionReady);
            Assert.True(true);
            // -----------------------------------------------------
        }

    }
}
