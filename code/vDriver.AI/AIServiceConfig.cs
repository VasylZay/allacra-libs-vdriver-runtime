﻿using System;

using vDriver.AI.Interface;

namespace vDriver.AI
{
    public class AIServiceConfig : IAIServiceConfig
    {
        public string AIJobsFolderPath { get; set; }
        public string AIS3BaseBucket { get; set; }
        public string AIS3WorkingFolder { get; set; }
        public int AIWorkersCount { get; set; }

        public string AI_RabbitMQ_QueueName { get; set; } // "ai_results_transferred"
    }
}
