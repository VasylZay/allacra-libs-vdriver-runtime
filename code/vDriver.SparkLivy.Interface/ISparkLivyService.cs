﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.SparkLivy.Interface
{
    public interface ISparkLivyService
    {
        ISparkLivyConfig SparkLivyConfig { get; }
        ILivyBatchJob CreateSparkLivyBacthJob();
    }
}
