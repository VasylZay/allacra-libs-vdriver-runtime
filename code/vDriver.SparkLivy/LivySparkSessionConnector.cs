﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;

using vDriver.Extensions;

namespace vDriver.SparkLivy
{
    public static class LivySparkSessionConnector 
    {
        // [VZ:TODO] Consider retry here:
        public static async Task<LivySession> CreateSessionAsync(LivySessionConfig config)
        {
            LivySession livySession = null;

            config = config ?? LivySessionConfig.Default;

            var request = new HttpRequestMessage(HttpMethod.Post, config.Url);

            // [VZ:REVISIT] Switch to specifying statement kind instead.
            var dataObject = new JObject();
            dataObject.Add("kind", LivySparkUtils.TranslateCodeKind(config.Kind));
            string data = dataObject.ToJsonString();

            using (var content = new StringContent(dataObject.ToJsonString(), Encoding.UTF8, "application/json"))
            {
                request.Content = content;

                HttpClient client = LivyHttpClientFactory.CreateClient(config.Url);

                var response = await client.PostAsync("sessions", content).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();

                var sessionPath = response.Headers.Location.AsRelativePath();

                livySession = new LivySession(client, config, sessionPath);
            }

            return livySession;
        }

    }
}
