﻿using System;
using System.Configuration;
using System.Reflection;
using System.IO;

using Microsoft.Extensions.Logging;

using vDriver.Interface;

namespace vDriver.Runtime
{

    public class AppConfig : IAppConfig
    {
        public string AppDirectory { get; set; }
        public string TempDirectory { get; set; }
    }

    public class vDriverConfig 
    {
        private static System.Configuration.Configuration config;
        private static ILogger Logger;

        static vDriverConfig()
        {
            Logger = vDriver.Library.AppLogging.CreateLogger<vDriverConfig>();
        }

        // [VZ:] Following suggestion here: https://stackoverflow.com/a/283917 otherwise it won't work with XUnit
        // cause XUnit assemblies run from a temporary folder.
        public vDriverConfig() 
        {
            // [VZ:REVISIT] We may want to consider vDriverWinService as below
            // Assembly service = Assembly.GetAssembly(typeof(vDriverWinService));
            Assembly service = Assembly.GetAssembly(typeof(vDriverApp));
            string codeBase = service.CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string vdriverPath = Uri.UnescapeDataString(uri.Path);

            config = ConfigurationManager.OpenExeConfiguration(vdriverPath);
        }


        #region vDriver AppConfig 

        public AppConfig GetAppConfig()
        {
            AppConfig appConfig = new AppConfig
            {
                AppDirectory = this.GetAppDirectory(),
            };

            return appConfig;
        }

        private string GetAppDirectory()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        private string GetTempDirectoryPath()
        {
            string dirPath = null;
            try
            {
                dirPath = Path.Combine(this.GetAppDirectory(), "TMP");
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Cannot create temp directory.");
            }

            return dirPath;
        }

        #endregion


        #region Web API

        internal const string KEYNAME_WebApi_Uri = "vDriver.WebApi.Url";
        public string GetWebApiBaseUri()
        {
            string setting = GetConfigurationValue(KEYNAME_WebApi_Uri);
            if (setting == null)
            {
                Logger.LogWarning("Base Uri is missing in config.");
            }

            return setting;
        }

        #endregion

        #region Win Service related settings

        internal const string KEYNAME_ServiceName = "WinServiceName";
        internal const string KEYNAME_ServiceDisplayName = "WinServiceDisplayName";
        internal const string KEYNAME_ServiceDescription = "WinServiceDescription";
        internal const string KEYNAME_ServiceEventLogName = "WinServiceEventLogName";
        internal const string KEYNAME_ServiceEventSourceName = "WinServiceEventSourceName";


        public static string GetServiceName()
        {
            string setting = GetConfigurationValue(KEYNAME_ServiceName);

            return (setting == null) ? "vDriver" : GetConfigurationValue(KEYNAME_ServiceName);
        }

        public static string GetServiceDisplayName()
        {
            string setting = GetConfigurationValue(KEYNAME_ServiceDisplayName);
            return (setting == null) ? "vDriver" : GetConfigurationValue(KEYNAME_ServiceDisplayName);
        }

        public static string GetServiceDescription()
        {
            string setting = GetConfigurationValue(KEYNAME_ServiceDescription);
            return (setting == null) ? "Alacra Spark vDriver Application" : GetConfigurationValue(KEYNAME_ServiceDescription);
        }

        public static string GetServiceEventLogName()
        {
            string setting = GetConfigurationValue(KEYNAME_ServiceEventLogName);
            return (setting == null) ? "VDRIVER" : GetConfigurationValue(KEYNAME_ServiceEventLogName);
        }

        public static string GetServiceEventSourceName()
        {
            string setting = GetConfigurationValue(KEYNAME_ServiceEventSourceName);
            return (setting == null) ? "VDRIVER" : GetConfigurationValue(KEYNAME_ServiceEventSourceName);
        }

        #endregion

        #region Utils

        internal static string GetConfigurationValue(string key)
        {
            string setting = null;

            try
            {
                if (config.AppSettings.Settings[key] != null)
                {
                    setting = config.AppSettings.Settings[key].Value;
                }
                else
                {
                    Logger.LogWarning("Missing AppSettings for the key: {0}", key);
                }
            }
            catch(Exception ex)
            {
                Logger.LogError(ex, "Exception in vDriverWinServiceSettings.GetConfigurationValue(..)");
            }

            return setting;
        }

        #endregion

    }
}
