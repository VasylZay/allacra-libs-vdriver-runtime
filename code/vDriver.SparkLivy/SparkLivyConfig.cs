﻿using System;
using vDriver.SparkLivy.Interface;


namespace vDriver.SparkLivy
{
    public class SparkLivyConfig : ISparkLivyConfig
    {
        public string ServerUrl { get; set; }
        public string ClassifierModelS3Path { get; set; }
        public string ClassifierJarS3Path { get; set; }
        public string ClassifierClassName { get; set; }
        public int SparkLivyBatchJobPollingDelay { get; set; }
        public int SparkLivySessionPollingDelay { get; set; }
    }

}
