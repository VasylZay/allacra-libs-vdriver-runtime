﻿
[1]
Install Amazon AWS SDK
======================

Install-Package AWSSDK.S3 -Version 3.3.102.17

[2]
Install Polly Package 
=====================
Landing page: https://www.nuget.org/packages/Polly/

Install-Package Polly -Version 7.1.0
