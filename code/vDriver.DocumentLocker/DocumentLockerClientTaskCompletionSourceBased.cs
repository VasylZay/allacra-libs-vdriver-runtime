﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace vDriver.DocumentLocker
{
    public class DocumentLockerClientTaskCompletionSourceBased
    {
        public TaskCompletionSource<string> RequestDocumentsAsync(string s3CommandLocation, string destinationLocation)
        {
            // THE SONNETS by William Shakespeare - 5.19 MB
            Uri address = new Uri("https://raw.githubusercontent.com/hortonworks/data-tutorials/master/tutorials/hdp/setting-up-a-spark-development-environment-with-scala/assets/shakespeare.txt");

            TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();

            using (WebClient webClient = new WebClient())
            {

                DownloadStringCompletedEventHandler handler = null;
                handler = (obj, args) =>
                {
                    webClient.DownloadStringCompleted -= handler;

                    if (args.Cancelled == true)
                    {
                        tcs.TrySetCanceled();
                        return;
                    }
                    else if (args.Error != null)
                    {
                        // Pass through to the underlying Task any exceptions thrown by the WebClient ??? // during the asynchronous operation.
                        tcs.TrySetException(args.Error);
                        return;
                    }
                    else
                    {
                        tcs.TrySetResult(args.Result);
                    }
                };

                webClient.DownloadStringCompleted += handler;
                webClient.DownloadStringAsync(address);
            }

            return tcs;
        }

    }
}
