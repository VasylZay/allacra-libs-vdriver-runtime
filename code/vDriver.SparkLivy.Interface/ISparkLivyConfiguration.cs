﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.SparkLivy.Interface
{
    public interface ISparkLivyConfig
    {
        string ServerUrl { get; }
        string ClassifierModelS3Path { get; }
        string ClassifierJarS3Path { get; }
        string ClassifierClassName { get; }
        int SparkLivyBatchJobPollingDelay { get; }
        int SparkLivySessionPollingDelay { get; }
    }
}
