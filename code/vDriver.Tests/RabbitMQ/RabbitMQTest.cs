﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using RabbitMQ.Client;

using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.RabbitMQ.Interface;


namespace vDriver.Tests
{
    public enum TestType { TestMessage, CannedJobs, PerformanceTest };

    public class RabbitMQTest : IRabbitMQTest
    {
        public const string _queueName = "ai_results_transferred";

        private bool _verbose = false;

        public RabbitMQTest(IAppService appService, ILogger<RabbitMQTest> logger, IRabbitMQService mqService)
        {
            this.AppService = appService;
            this.Logger = logger;
            this.RabbitMQService = mqService;
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }
        public IRabbitMQService RabbitMQService { get; private set; }

        public async Task<string> SendTestMessage()
        {
            return await Task.Run<string>(() => this.RunTestInternal(TestType.TestMessage));
        }

        public async Task<string> SendCannedJobs()
        {
            return await Task.Run<string>(() => this.RunTestInternal(TestType.CannedJobs));
        }

        public async Task<string> SendMessagesForPerformanceTest(int messageCount)
        {
            return await Task.Run<string>(() => this.RunTestInternal(TestType.PerformanceTest, messageCount));
        }

        private string RunTestInternal(TestType testType, int messageCount = 10)
        {
            string rslt = string.Empty;

            string msg = string.Format("Sending RabbitMQ {0} messages immitating Clarity Combatch funnctionality", messageCount);
            this.Logger.LogInformation(msg);

            IRabbitMQConfig config = this.RabbitMQService.RabbitMQConfig;
            string hostname = config.ServerHost;

            var factory = new ConnectionFactory() { HostName = hostname };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: _queueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                int cnt = 0;
                switch (testType)
                {
                    case TestType.PerformanceTest:
                        cnt = this.SendTestMessages(channel, properties, messageCount);
                        break;

                    case TestType.CannedJobs:
                        cnt = this.SendCannedJobs(channel, properties);
                        break;

                    case TestType.TestMessage:
                        cnt = this.SendTestMessage(channel, properties);
                        break;

                }

                //int cnt = SendMessagesBunch(channel, properties);
                //int cnt = SendMessageForCannedJob(channel, properties, "message_11.json");

                msg = string.Format("Sent {0} messages to RabbitMQ brocker.", cnt);
                Logger.LogInformation(msg);
            }

            return rslt;
        }

        private int SendTestMessage(IModel channel, IBasicProperties properties)
        {
            int cnt = 0;

            string dataPath = Path.Combine(this.AppService.GetAppConfig().AppDirectory, "RabbitMQ", "test-messages");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");
            if (fileList.Length > 0)
            {
                string payload = File.ReadAllText(fileList[0]);


                if (SendMessage(channel, properties, payload))
                {
                    cnt++;
                }
            }

            return cnt;
        }

        protected bool SendMessage(IModel channel, IBasicProperties properties, string payload)
        {
            bool rslt = false;

            try
            {
                if (VerifyMessagePayload(payload))
                {
                    AIRequestMessageEnvelope message = JsonConvert.DeserializeObject<AIRequestMessageEnvelope>(payload);

                    var body = Encoding.UTF8.GetBytes(payload);
                    channel.BasicPublish(exchange: "", // this is default - nameless exchange
                                         routingKey: _queueName,
                                         basicProperties: properties,
                                         body: body);

                    if (this._verbose)
                    {
                        string msg = String.Format("RabbitMQ message was sent:\nmessage id={0}, \nrequest_rollup_batch.id={1}\n", message.message_id, message.request_rollup_batch.id);
                        this.Logger.LogInformation(msg);
                    }

                    rslt = true;
                }
                else
                {
                    this.Logger.LogError("Cannot send RabbitMQ message: wrong message format");
                }
            }
            catch (Exception e)
            {
                this.Logger.LogError(e, "Cannot send RabbitMQ message");
            }

            return rslt;
        }

        protected bool VerifyMessagePayload(string payload)
        {
            bool rslt = false;

            try
            {
                AIRequestMessage message = JsonConvert.DeserializeObject<AIRequestMessage>(payload);

                Guid rollupId = message.request_rollup_batch.id;
                int cnt = message.request_rollup_batch.documents.Count();
                rslt = true;
            }
            catch (Exception e)
            {
                this.Logger.LogError(e, "Cannot deserialize RabbitMQ message.");
            }

            return rslt;
        }

        private int SendTestMessages(IModel channel, IBasicProperties properties, int messageCount)
        {
            int cnt = 0;

            string dataPath = Path.Combine(this.AppService.GetAppConfig().AppDirectory, "RabbitMQ", "test-messages");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");
            int messagesToSent = messageCount;
            while (cnt < messageCount)
            {
                int sentBanch = this.SendMessagesBunch(channel, properties, messagesToSent);
                cnt = cnt + sentBanch;
                messagesToSent = messagesToSent - sentBanch;
            }

            return cnt;
        }

        private int SendMessagesBunch(IModel channel, IBasicProperties properties, int messageCountCap = int.MaxValue)
        {
            int cnt = 0;

            string dataPath = Path.Combine(this.AppService.GetAppConfig().AppDirectory, "RabbitMQ", "test-messages");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");

            for (int i = 0; i < fileList.Length; i++)
            {
                string payload = File.ReadAllText(fileList[i]);
                if (cnt < messageCountCap)
                {
                    if (SendMessage(channel, properties, payload))
                    {
                        cnt++;
                    }
                }
                else
                {
                    break;
                }
            }

            return cnt;
        }

        private int SendCannedJobs(IModel channel, IBasicProperties properties)
        {
            int cnt = 0;

            string dataPath = Path.Combine(this.AppService.GetAppConfig().AppDirectory, "RabbitMQ", "cannedjobs");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");
            for (int i = 0; i < fileList.Length; i++)
            {
                string payload = File.ReadAllText(fileList[i]);
                if (SendMessage(channel, properties, payload))
                {
                    cnt++;
                }
            }

            return cnt;
        }



        private int SendCannedJob(IModel channel, IBasicProperties properties, string fileName)
        {
            int cnt = 0;

            string dataPath = Path.Combine(this.AppService.GetAppConfig().AppDirectory, "RabbitMQ", "cannedjobs");
            string filePath = Path.Combine(dataPath, fileName);
            if (File.Exists(filePath))
            {
                string payload = File.ReadAllText(filePath);
                if (SendMessage(channel, properties, payload))
                {
                    cnt++;
                }
            }

            return cnt;
        }

    }
}
