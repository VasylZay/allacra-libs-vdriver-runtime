﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace vDriver.AI
{
    public class AITask
    {

        public AITask(Guid jobId, string messagePayload, Guid messageId, DateTime timeStamp, Guid rollupId, string serviceId)
        {
            this.JobId = jobId;
            this.MessagePayload = messagePayload;
            this.MessageId = messageId;
            this.TimeStamp = timeStamp;
            this.RollupId = rollupId;
            this.ServiceId = serviceId;
        }

        public Guid JobId { get; private set; }
        public Guid MessageId { get; private set; }
        public DateTime TimeStamp { get; private set; }
        public Guid RollupId { get; private set; }
        public string ServiceId { get; private set; }

        [JsonIgnore]
        public string MessagePayload { get; private set; }

        [JsonIgnore]
        public string LocalDirPath { get; set; }


        [JsonIgnore]
        public string BacketName { get; set; }

        [JsonIgnore]
        public string AIWorkingFolder { get; set; }

        [JsonIgnore]
        public string JobFolder { get; set; }

        [JsonIgnore]
        public string DataSubfolder { get; set; }

        [JsonIgnore]
        public string ResultsSubfolder { get; set; }
        
        [JsonIgnore]
        public string S3CommandPath { get; set; }

        [JsonIgnore]
        public string S3DataPath { get; set; }

        [JsonIgnore]
        public string S3ResultsPath { get; set; }

    }

}
