﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using RabbitMQ.Client;

using vDriver.AI.Interface;



namespace vDriver.Test.ClaritySendMockup
{
    class Sender
    {
        // [VZ:HOTSPOT]
        public static int _totalMessagesCount = 100;

        public const string _queueName = "ai_results_transferred";

        private static bool _verbose = false;

        public static void Main(string[] args)
        {
            Console.WriteLine("Sending RabbitMQ messages immitating Clarity Combatch funnctionality.\n");

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: _queueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                //int cnt = SendTestMessage(channel, properties);
                //int cnt = SendMessagesBunch(channel, properties);
                //int cnt = SendTestMessages(channel, properties, _totalMessagesCount);

                int cnt = SendMessageForCannedJob(channel, properties, "message_11.json");

                Console.WriteLine("Sent {0} messages to RabbitMQ brocker.", cnt);
            }

            Console.WriteLine("Exiting RabbitMQ Send app...\n\n");

            //Console.WriteLine("Press [Enter] to exit.");
            Console.ReadLine();
        }

        #region canned AI Jobs

        private static int SendMessageForCannedJob(IModel channel, IBasicProperties properties, string fileName)
        {
            int cnt = 0;

            string dataPath = Path.Combine(AppDirectory, "data", "cannedjobs");
            string filePath = Path.Combine(dataPath, fileName);
            if (File.Exists(filePath))
            {
                string payload = File.ReadAllText(filePath);
                if (SendMessage(channel, properties, payload))
                {
                    cnt++;
                }
            }

            return cnt;
        }

        #endregion


        private static int SendTestMessage(IModel channel, IBasicProperties properties)
        {
            int cnt = 0;

            string dataPath = Path.Combine(AppDirectory, "data");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");
            if(fileList.Length > 0)
            {
                string payload = File.ReadAllText(fileList[0]);


                if (SendMessage(channel, properties, payload))
                {
                    cnt++;
                }
            }

            return cnt;
        }


        private static int SendTestMessages(IModel channel, IBasicProperties properties, int messageCount)
        {
            int cnt = 0;

            string dataPath = Path.Combine(AppDirectory, "data");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");
            int messagesToSent = messageCount;
            while (cnt < messageCount)
            {
                int sentBanch = SendMessagesBunch(channel, properties, messagesToSent);
                cnt = cnt + sentBanch;
                messagesToSent = messagesToSent - sentBanch;
            }

            return cnt;
        }


        private static int SendMessagesBunch(IModel channel, IBasicProperties properties, int messageCountCap = int.MaxValue)
        {
            int cnt = 0;

            string dataPath = Path.Combine(AppDirectory, "data");
            string[] fileList = Directory.GetFiles(dataPath, "message_*.json");

            for (int i = 0; i < fileList.Length; i++)
            {
                string payload = File.ReadAllText(fileList[i]);
                if (cnt < messageCountCap)
                {
                    if (SendMessage(channel, properties, payload))
                    {
                        cnt++;
                    }
                }
                else
                {
                    break;
                }
            }

            return cnt;
        }

        private static bool SendMessage(IModel channel, IBasicProperties properties, string payload)
        {
            bool rslt = false;

            try
            {
                if (VerifyMessagePayload(payload))
                {
                    AIRequestMessageEnvelope message = JsonConvert.DeserializeObject<AIRequestMessageEnvelope>(payload);

                    var body = Encoding.UTF8.GetBytes(payload);
                    channel.BasicPublish(exchange: "", // this is default - nameless exchange
                                         routingKey: _queueName,
                                         basicProperties: properties,
                                         body: body);

                    if (_verbose)
                    {
                        string msg = String.Format("RabbitMQ message was sent:\nmessage id={0}, \nrequest_rollup_batch.id={1}\n", message.message_id, message.request_rollup_batch.id);
                        Console.WriteLine(msg);
                    }

                    rslt = true;
                }
                else
                {
                    Console.WriteLine("Cannot process RabbitMQ message: wrong message format");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot send RabbitMQ message. EXCEPTION:\n" + e.Message);
            }

            return rslt;
        }

        private static bool VerifyMessagePayload(string payload)
        {
            bool rslt = false;

            try
            {
                AIRequestMessage message = JsonConvert.DeserializeObject<AIRequestMessage>(payload);

                Guid rollupId = message.request_rollup_batch.id;
                int cnt = message.request_rollup_batch.documents.Count();
                rslt = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot deserialize RabbitMQ message. EXCEPTION:\n" + e.Message);
            }

            return rslt;
        }

        public static string AppDirectory
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }
    }
}
