﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace vDriver.Library
{
    public static class Util
    {
        public static Tuple<string, string> DumpThreadPoolInfo()
        {
            int workerThreads;
            int portThreads;

            ThreadPool.GetMaxThreads(out workerThreads, out portThreads);
            string maxWorkerThreads = string.Format("\nMaximum worker threads: \t{0}" +
                "\nMaximum completion port threads: {1}",
                workerThreads, portThreads);


            ThreadPool.GetAvailableThreads(out workerThreads,
                out portThreads);
            string availableWorkerThreads = string.Format("\nAvailable worker threads: \t{0}" +
                "\nAvailable completion port threads: {1}\n",
                workerThreads, portThreads);

            return new Tuple<string, string>(maxWorkerThreads, availableWorkerThreads);

        }
    }
}
