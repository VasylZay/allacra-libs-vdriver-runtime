﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.DocumentLocker.Interface
{
    public interface IDocumentLockerTest
    {
        Task<string> Run();
    }
}
