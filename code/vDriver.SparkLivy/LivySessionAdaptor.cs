﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.LivySpark
{
    public class LivySessionAdaptor : IDisposable
    {
        private LivySession _session;

        public async Task<string> ExecuteSparkStatementAsync(string code)
        {
            string rslt = string.Empty;

            if(this._session == null)
            {
                this._session = await this.CreateSessionAsync();
            }

            if(this._session == null)
            {
                rslt = "Cannot establish Livy Spark session";
            }
            else
            {
                rslt = await this._session.ExecuteStatementAsync(code);
            }

            return rslt;
        }

        public async Task<string> ExecutePySparkStatementAsync(string code)
        {
            string rslt = string.Empty;

            if (this._session == null)
            {
                this._session = await this.CreateSessionAsync();
            }

            if (this._session == null)
            {
                rslt = "Cannot establish Livy Spark session";
            }
            else
            {
                rslt = await this._session.ExecuteStatementAsync(code, SparkCodeKind.PySpark);
            }

            return rslt;
        }

        private async Task<LivySession> CreateSessionAsync()
        {
            LivySession session = null;

            LivySessionConfig config = new LivySessionConfig();
            config.Url = vDriverConfig.GetLivySparkClusterUrl();
            config.Kind = SparkCodeKind.Spark;

            Logger.Trace($"[{config.Name}] Creating session...");
            session = await LivySparkSessionConnector.CreateSessionAsync(config);

            return session;
        }

        public async Task CloseSessionAsync()
        {
            if(this._session != null)
            {
                try
                {
                    await this._session.CloseAsync();
                }
                catch { }
            }
        }


        // [VZ:REVISIT] Remove Dispose
        #region Dispose

        ~LivySessionAdaptor()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._session != null)
                {
                    this._session.Dispose();
                }
            }
        }

        #endregion

    }
}
