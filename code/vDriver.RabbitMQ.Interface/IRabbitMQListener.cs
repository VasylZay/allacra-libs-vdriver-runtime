﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RabbitMQ.Client.Events;

namespace vDriver.RabbitMQ.Interface
{
    public interface IRabbitMQListener : IDisposable
    {
        void Start();
        void RegisterCallbacks(Func<BasicDeliverEventArgs, Task> doWorkCallback, Action<string> cancelWorkCallback);
        void AcknowledgeMessageDelivery(ulong deliveryTag);
    }
}
