﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.RabbitMQ.Interface
{
    /*
        ContinuationTimeout	{00:00:20}	System.TimeSpan
		DispatchConsumersAsync	false	bool
		HandshakeContinuationTimeout	{00:00:10}	System.TimeSpan
		RequestedFrameMax	0	uint
    */

    public interface IRabbitMQConfig
    {
        ushort Port { get; }
        string ServerHost { get; }
        string UserName { get; }
        string Password { get; }
        bool AutomaticRecoveryEnabled { get; }
        TimeSpan NetworkRecoveryInterval { get; }
        int RequestedConnectionTimeout { get; }
        ushort RequestedHeartbeat { get; }
    }
}
