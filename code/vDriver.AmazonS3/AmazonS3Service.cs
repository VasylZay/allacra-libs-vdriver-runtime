﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.IO;
using System.Net;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.Runtime;

using vDriver.AWS;
using vDriver.AmazonS3.Interface;

namespace vDriver.AmazonS3
{
    public class AmazonS3Service : vDriver.AmazonS3.Interface.IAmazonS3Service, IDisposable
    {

        private readonly SemaphoreSlim _mutex = new SemaphoreSlim(1, 1);

        private readonly AWSCredentials _awsCredentials;
        private readonly RegionEndpoint _regionEndpoint;
        private readonly AmazonS3Client _masterS3Client;

        public AmazonS3Service(ILogger<AmazonS3Service> logger, IOptions<AmazonS3Config> config)
        {
            this.Logger = logger;
            this.AmazonS3Configuration = config.Value;

            this._awsCredentials = AWSUtil.GetAWSCredentialsUsingProfileName(this.AmazonS3Configuration.AWSCredentialProfileName);
            if (this._awsCredentials == null)
            {
                string msg = "FATAL ERROR: Cannot obtain AWS credentials for the AWS User Profile: " + this.AmazonS3Configuration.AWSCredentialProfileName;
                logger.LogCritical(msg);

                throw new Exception(msg);
            }

            this._regionEndpoint = AWSUtil.GetRegionEndpoint(this.AmazonS3Configuration.AWSRegion);
            if(this._regionEndpoint == null)
            {
                string msg = "FATAL ERROR: Cannot connect to Amazon S3 service - unknown Endpoint.";
                logger.LogCritical(msg);

                throw new Exception(msg);
            }

            this._masterS3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint);

            //logger.LogInformation("AmazonS3Service was created successfully.");
        }

        public ILogger Logger { get; private set; }
        public IAmazonS3Config AmazonS3Configuration { get; private set; }


        #region IAmazonS3Service

        public async Task<string> FormatAndVerifyS3Path(string bucketName, string folderName, string prefix)
        {
            string rslt = null;

            try
            {
                if (await this.CreateDirectoryAsync(bucketName, folderName, prefix))
                {
                    rslt = String.Format("s3://{0}", bucketName);
                    rslt = Path.Combine(rslt, folderName, prefix);
                    rslt = rslt.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "Failed to verify S3 path");
            }

            return rslt;
        }

        public async Task<bool> ConnectAsync()
        {
            bool isConnected = false;

            try
            {
                // [VZ:TODO] Use Polly here: implement retry.
                ListBucketsResponse rsps = await this._masterS3Client.ListBucketsAsync();
                if (rsps.HttpStatusCode == HttpStatusCode.OK)
                {
                    isConnected = true;
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "Failed to connect to AWS S3 service.");
            }

            return isConnected;
        }

        // [VZ:] Idempotent semantics: bucket will only be created if it doesn't exist already.
        public async Task<bool> CreateBucketAsync(string bucketName)
        {
            bool rslt = false;

            if (await this.DoesBucketExist(bucketName))
            {
                rslt = true;
            }
            else
            {
                await this._mutex.WaitAsync();
                try
                {
                    PutBucketResponse rsps = await this._masterS3Client.PutBucketAsync(bucketName).ConfigureAwait(false);
                    if (rsps.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    {
                        this.Logger.LogInformation(String.Format("Bucket named '{0}' was created successfully.", bucketName));

                        HttpStatusCode statusCode = await this._masterS3Client.BlockPublicAccess(bucketName).ConfigureAwait(false);
                        if (statusCode == HttpStatusCode.OK)
                        {
                            this.Logger.LogInformation(String.Format("Public acceess for bucket named '{0}' was blocked.", bucketName));
                        }

                        rslt = await this._masterS3Client.WaitForBucketToBeAvailable(bucketName);
                    }

                    // [DEBUG]
                    // bool checkExists = await AmazonS3Util.DoesS3BucketExistAsync(this._masterS3Client, bucketName);
                }
                catch (Exception ex)
                {
                    string msg = String.Format("Failed to create S3 bucket {0}.", bucketName);
                    this.Logger.LogError(ex, msg);
                }
                finally
                {
                    this._mutex.Release();
                }
            }
        
            return rslt;
        }

        public async Task<bool> DeleteBucketAsync(string bucketName)
        {
            bool rslt = false;

            try
            {
                DeleteBucketResponse delRsps = await this._masterS3Client.DeleteBucketAsync(bucketName).ConfigureAwait(false);
                if (delRsps.HttpStatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    rslt = true;
                }
            }
            catch(Amazon.S3.AmazonS3Exception ex)
            {
                string msg = "Cannot delete S3 bucket";
                this.Logger.LogError(ex, msg);
            }
            catch (Exception ex)
            {
                string msg = "Cannot delete S3 bucket";
                this.Logger.LogError(ex, msg);
            }

            return rslt;
        }

        public async Task<bool> DoesBucketExist(string bucketName)
        {
            return await this._masterS3Client.DoesBucketExist(bucketName);
        }

        public async Task<List<S3Bucket>> ListBuckets()
        {
            return await this._masterS3Client.GetBucketList().ConfigureAwait(false);
        }

        // [VZ:] Will create directory on the root level:
        public async Task<bool> CreateDirectoryAsync(string bucketName, string dirName)
        {
            bool rslt = false;

            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                rslt = await Task.Run(() => s3Client.CreateDirectory(bucketName, dirName));
            }

            return rslt;
        }

        public async Task<bool> CreateDirectoryAsync(string bucketName, string parentDirKey, string dirName)
        {
            bool rslt = false;

            dirName = Path.Combine(parentDirKey, dirName);
            dirName = dirName.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                rslt = await Task.Run(() => s3Client.CreateDirectory(bucketName, dirName));
            }

            return rslt;
        }

        // [VZ:] Will delete directory recursively:
        public async Task<bool> DeleteDirectoryAsync(string bucketName, string dirKey)
        {
            bool rslt = false;

            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                rslt = await Task.Run(() => s3Client.DeleteDirectory(bucketName, dirKey));
            }

            return rslt;
        }

        // [VZ:] This method will return all directory keys below the root level for given bucket:
        public async Task<List<string>> ListDirectoryKeysAsync(string bucketName)
        {
            List<string> list = new List<string>();

            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                if (await s3Client.DoesBucketExist(bucketName))
                {
                    S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, bucketName);
                    if(dirInfo.Exists)
                    {
                        IEnumerable<S3DirectoryInfo> dirSeqs = dirInfo.EnumerateDirectories();
                        foreach(S3DirectoryInfo t in dirSeqs)
                        {
                            list.Add(t.Name);
                        }
                    }
                }
            }

            return list;
        }

        // [VZ:] This method will return all directory keys below the specified directory in a given bucket:
        public async Task<List<string>> ListDirectoryKeysAsync(string bucketName, string directoryKey)
        {
            List<string> list = new List<string>();

            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                if (await s3Client.DoesBucketExist(bucketName))
                {
                    S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, bucketName, directoryKey);
                    if (dirInfo.Exists)
                    {
                        IEnumerable<S3DirectoryInfo> dirSeqs = dirInfo.EnumerateDirectories();
                        foreach (S3DirectoryInfo t in dirSeqs)
                        {
                            list.Add(t.Name);
                        }
                    }
                }
            }

            return list;
        }


        // [VZ:] This will recursivly enumerate all objects in the bucket or create a new bucket if bucket does not exist:
        public async Task<List<S3Object>> ListObjectsAsync(string bucketName)
        {
            List<S3Object> list = new List<S3Object>();

            if (await this.DoesBucketExist(bucketName))
            {
                using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
                {
                    list = await AmazonS3ClientExtensions.ListObjectsAsync(s3Client, bucketName);
                }
            }
            else
            {
                await this.CreateBucketAsync(bucketName);
            }

            return list;
        }

        public async Task<bool> CopyObjectAsync(string sourceBucket, string objectKey, string destinationBucket, string destObjectKey)
        {
            bool rslt = false;

            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                CopyObjectRequest request = new CopyObjectRequest
                {
                    SourceBucket = sourceBucket,
                    SourceKey = objectKey,
                    DestinationBucket = destinationBucket,
                    DestinationKey = destObjectKey
                };

                try
                {
                    CopyObjectResponse response = await s3Client.CopyObjectAsync(request);
                    HttpStatusCode rsps = response.HttpStatusCode;
                    if(response.HttpStatusCode == HttpStatusCode.OK)
                    {
                        rslt = true;
                    }
                }
                catch (Exception ex)
                {
                    string msg = "Cannot copy S3 object";
                    this.Logger.LogError(ex, msg);
                }
            }

            return rslt;
        }

        public async Task DownloadFileAsync(string targetFilePath, string bucketName, string sourceKeyName)
        {
            if (await this.DoesBucketExist(bucketName))
            {
                using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
                using (var fileTransferUtility = new TransferUtility(s3Client))
                {
                    await fileTransferUtility.DownloadAsync(targetFilePath, bucketName, sourceKeyName);
                }
            }
        }


        public async Task UploadFileAsync(string sourceFilePath, string bucketName, string targetKeyName)
        {
            if (await this.DoesBucketExist(bucketName))
            {
                using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
                using(var fileTransferUtility = new TransferUtility(s3Client))
                {
                    await fileTransferUtility.UploadAsync(sourceFilePath, bucketName, targetKeyName);
                }
            }
        }

        public async Task UploadDirAsync(string sourceDirPath, string bucketName, string prefix)
        {
            await this.UploadDirAsync(sourceDirPath, bucketName, prefix, new CancellationToken());
        }

        public async Task UploadDirAsync(string sourceDirPath, string bucketName, string prefix, CancellationToken cancellationToken)
        {
            if (await this.DoesBucketExist(bucketName))
            {
                using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
                using (var transferUtility = new TransferUtility(s3Client))
                {
                    var transferRqst = new TransferUtilityUploadDirectoryRequest
                    {
                        BucketName = bucketName,
                        KeyPrefix = prefix, 
                        Directory = sourceDirPath,
                        SearchOption = SearchOption.AllDirectories
                    };

                    await transferUtility.UploadDirectoryAsync(transferRqst, cancellationToken);
                }
            }
        }

        public async Task DownloadDirectoryAsync(String bucketName, String s3Directory, String localDirectory)
        {
            await DownloadDirectoryAsync(bucketName, s3Directory, localDirectory, new CancellationToken());
        }

        public async Task DownloadDirectoryAsync(String bucketName, String s3Directory, String localDirectory, CancellationToken cancellationToken)
        {
            if (await this.DoesBucketExist(bucketName))
            {
                using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
                using (var transferUtility = new TransferUtility(s3Client))
                {
                    var transferRqst = new TransferUtilityDownloadDirectoryRequest
                    {
                        BucketName = bucketName,
                        DownloadFilesConcurrently = true,
                        LocalDirectory = localDirectory,
                        S3Directory = s3Directory
                    };

                    await transferUtility.DownloadDirectoryAsync(transferRqst, cancellationToken);
                }
            }

        }



        // [VZ:] This is scratch method:
        public async Task __Test()
        {
            await Task.Delay(10);

            string bucketName = "vz-bucket-test-east-2";
            using (var s3Client = new AmazonS3Client(this._awsCredentials, this._regionEndpoint))
            {
                //s3://vz-bucket-test-east-2/Zzz/Aaa/about.txt
                //S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, bucketName);
                S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, bucketName, @"Zzz\Aaa");

                string fullName = dirInfo.FullName;

                //S3DirectoryInfo dirInfo2 = dirInfo.GetDirectory("Aaa");
                //var zzz = await Task.Run(() => dirInfo.GetFiles());

                try
                {
                    dirInfo.Delete(true);
                }
                catch(Exception e)
                {
                    string msg = e.Message;
                }

               //var zzz = await ListingObjectsAsync(s3Client, bucketName);
            }

        }

        #endregion IAmazonS3Service


        #region Dispose

        ~AmazonS3Service()
        {
            Dispose(false);
        }

        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {

            if (disposing && (this._masterS3Client != null))
            {
                this._masterS3Client.Dispose();
            }
        }

        #endregion

    }
}
