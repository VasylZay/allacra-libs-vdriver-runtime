﻿namespace vDriver.AmazonS3.Interface
{
    public interface IAmazonS3Config
    {
        string AWSRegion { get; }
        string AWSCredentialProfileName { get; }
    }
}
