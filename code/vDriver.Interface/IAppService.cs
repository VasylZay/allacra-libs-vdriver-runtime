﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.Interface
{
    public interface IAppService
    {
        event EventHandler ShutdownApplication;

        IServiceProvider GetServiceProvider();
        IAppConfig GetAppConfig();
        void ShutDownApplication();
    }
}
