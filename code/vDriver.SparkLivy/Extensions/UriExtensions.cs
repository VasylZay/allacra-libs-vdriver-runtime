﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.SparkLivy
{
    public static class UriExtensions
    {
        // [VZ:TODO] Simplify if possible
        // TODO: Make it non-crap magic 1 number substring
        // Remove leading /
        public static string AsRelativePath(this Uri uri) => uri.ToString().Substring(1);
    }
}
