﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using vDriver.RabbitMQ.Interface;

namespace vDriver.RabbitMQ
{
    public class RabbitMQService : IRabbitMQService
    {
        private readonly IRabbitMQResilientConnection _connection;

        public RabbitMQService(ILogger<RabbitMQService> logger, IOptions<RabbitMQConfig> config)
        {
            this.Logger = logger;
            this.RabbitMQConfig = config.Value;

            this._connection = new RabbitMQResilientConnection(logger, this.RabbitMQConfig);
        }

        public ILogger Logger { get; private set; }
        public IRabbitMQConfig RabbitMQConfig { get; private set; }

        public async Task<IRabbitMQResilientConnection> GetConnectionAsync()
        {
            await this._connection.InitializeAsync();

            return this._connection;
        }

        public IRabbitMQListener CreateRabbitMQListener(IRabbitMQResilientConnection connection, string queueName)
        {
            return new RabbitMQListener(this.Logger, connection, queueName);
        }

    }
}


