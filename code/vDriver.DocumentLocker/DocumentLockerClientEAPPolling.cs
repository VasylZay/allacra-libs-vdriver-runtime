﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.DocumentLocker
{
    // [VZ:] At the moment this is a mock for the DocumentLockerClient that will be using EAP = Event-based Asynchronous Pattern
    // using polling. For now it will retrieve large document using WebClient.
    
    public class DocumentLockerClientEAPPolling : IDisposable
    {

        public event EventHandler RequestCompleted;

        private readonly WebClient _webClient;
        private DownloadStringCompletedEventArgs _rslt = null;

        public DocumentLockerClientEAPPolling()
        {
            this._webClient = new WebClient();
            this._webClient.DownloadStringCompleted += WebClient_DownloadStringCompleted;
        }

        public bool SuccessFlag
        {
            get
            {
                bool isSuccess = false;

                if (this._rslt != null)
                {
                    if(this._rslt.Error == null)
                    {
                        isSuccess = true;
                    }
                }

                return isSuccess;
            }
        }


        public string ErrorMessage
        {
            get
            {
                string message = string.Empty;

                if(this._rslt == null)
                {
                    message = "Failed to request documents from DocumentLocker";
                }
                else if(this._rslt.Error != null)
                {
                    message = "Failed to request documents from DocumentLocker." + Environment.NewLine + "EXCEPTION: " + this._rslt.Error.Message;
                }

                return message;
            }
        }

        public string Result
        {
            get
            {
                return (this._rslt == null) ? string.Empty : this._rslt.Result;
            }
        }


        public void RequestDocumentsAsync(string s3CommandLocation, string destinationLocation)
        {
            // THE SONNETS by William Shakespeare - 5.19 MB
            Uri address = new Uri("https://raw.githubusercontent.com/hortonworks/data-tutorials/master/tutorials/hdp/setting-up-a-spark-development-environment-with-scala/assets/shakespeare.txt");

            this._webClient.DownloadStringAsync(address);
        }

        protected virtual void OnRequestCompleted(DownloadStringCompletedEventArgs args)
        {
            this.RequestCompleted?.Invoke(this, args);
        }

        private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs args)
        {
            this._rslt = args;
            this.OnRequestCompleted(args);
        }

        #region Dispose

        ~DocumentLockerClientEAPPolling()
        {
            Dispose(false);
        }

        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(this._webClient != null)
            {
                this._webClient.DownloadStringCompleted -= WebClient_DownloadStringCompleted;
                this._webClient.Dispose();
            }
        }

        #endregion

    }
}
