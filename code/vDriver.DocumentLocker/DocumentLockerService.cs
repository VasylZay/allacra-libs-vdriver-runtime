﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using Amazon.Runtime;

using vDriver.Interface;
using vDriver.AWS;
using vDriver.DocumentLocker.Interface;
using vDriver.AmazonS3.Interface;


namespace vDriver.DocumentLocker
{
    public class DocumentLockerService : IDocumentLockerService
    {
        private static string _cannedJobsFileName = "canned-ai-jobs.json";

        private readonly Dictionary<Guid, string> _cannedAIJobs;

        public DocumentLockerService(IAppService appService, ILogger<DocumentLockerService> logger, IOptions<DocumentLockerConfig> config, IAmazonS3Service amazonS3Service)
        {
            this.AppService = appService;
            this.Logger = logger;
            this.DocumentLockerConfig = config.Value;
            this.AmazonS3Service = amazonS3Service;

            this._cannedAIJobs = this.GetCannedJobs();
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }
        public IDocumentLockerConfig DocumentLockerConfig { get; private set; }
        public IAmazonS3Service AmazonS3Service { get; private set; }

  
        // [VZ:DEVTIME] This is to test different ways to deal with long running remote calls:
        public async Task<Tuple<bool, string>> RequestDocumentsExperimental(string s3CommandLocation, string destinationLocation)
        {
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string msg = string.Format("DocumentLockerService is running on the managed thread, id = {0}", hndl);
            this.Logger.LogInformation(msg);

            DocumentLockerWorkerExperimental wrkr = new DocumentLockerWorkerExperimental(this.AppService, this.Logger);
            return await wrkr.RequestDocumentsAsync(s3CommandLocation, destinationLocation);
        }


        // [VZ:DEVTIME] This is to test AWS lambda created by Zach
        public async Task<string> CallLambdaExperimental()
        {
            string payload = null;

            try
            {
                string region = this.DocumentLockerConfig.AWSRegion;
                string profileName = this.DocumentLockerConfig.AWSCredentialProfileName;
                RegionEndpoint regionEndpoint = AWSUtil.GetRegionEndpoint(region);
                AWSCredentials awsCredentials = AWSUtil.GetAWSCredentialsUsingProfileName(profileName);

                AmazonLambdaConfig lambdaConfig = new AmazonLambdaConfig();
                TcpKeepAlive tcpKeepAlive = lambdaConfig.TcpKeepAlive;

                AmazonLambdaClient client = new AmazonLambdaClient(awsCredentials, regionEndpoint);
                

                InvokeResponse res = await client.InvokeAsync(
                    new InvokeRequest
                    {
                        FunctionName = "arn:aws:lambda:us-east-1:627761140844:function:FunctionHandler",
                        Payload = "{\"command\":\"get\",\"rollup_id\":\"47040426-28B3-48FA-9D57-0000287F8B0C\",\"backpack_type\":\"EntityExtraction\"}"
                    });

                payload = await (new StreamReader(res.Payload)).ReadToEndAsync();
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "Falied to call Amazon lambda!");
            }

            return payload;
        }


        // [VZ:CLEANUP] Cleanup Guid rollupId evenually: it is used only for Document Locker mockup while it is under development:
        public async Task<Tuple<bool, string>> RequestDocuments(string s3CommandLocation, string destinationLocation, Guid rollupId)
        {
            // [VZ:CLEANUP]
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string msg = string.Format("DocumentLockerService is running on the managed thread, id = {0}", hndl);
            this.Logger.LogInformation(msg);

            Tuple<bool, string> rslt = null;

            if (_cannedAIJobs.ContainsKey(rollupId))
            {
                // look-up source location:
                s3CommandLocation = _cannedAIJobs[rollupId];

                DocumentLockerWorkerForCannedJob wrkr = new DocumentLockerWorkerForCannedJob(this.AppService, this.Logger, this.AmazonS3Service);
                rslt = await Task.Run< Tuple<bool, string>>(() => wrkr.RequestDocumentsAsync(s3CommandLocation, destinationLocation));
            }
            else
            {
                rslt = new Tuple<bool, string>(false, "Implement DocumentLockerWorker");
            }

            return rslt;
        }


        private Dictionary<Guid, string> GetCannedJobs()
        {
            Dictionary<Guid, string> dict = new Dictionary<Guid, string>();

            List <CannedAIJob> list = new List<CannedAIJob>();
            try
            {
                string payload = System.IO.File.ReadAllText(DocumentLockerService._cannedJobsFileName);
                list = JsonConvert.DeserializeObject<List<CannedAIJob>>(payload);
                foreach (CannedAIJob kvp in list)
                {
                    dict.Add(kvp.RollupId, kvp.S3DocumentSetPath);
                }
            }
            catch(Exception ex)
            {
                this.Logger.LogWarning(ex, "Failed to get canned job list");
            }

            return dict;
        }
    }

    public class CannedAIJob
    {
        public Guid RollupId { get; set; }
        public string S3DocumentSetPath { get; set; }
    }

}