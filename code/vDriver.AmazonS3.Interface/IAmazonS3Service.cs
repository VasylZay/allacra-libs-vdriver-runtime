﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Amazon.S3;
using Amazon.S3.Model;


namespace vDriver.AmazonS3.Interface
{
    public interface IAmazonS3Service
    {
        Task<string> FormatAndVerifyS3Path(string bucketName, string folderName, string prefix);

        Task<bool> ConnectAsync();
        Task<bool> CreateBucketAsync(string bucketName);
        Task<bool> DeleteBucketAsync(string bucketName);
        Task<bool> DoesBucketExist(string bucketName);
        Task<bool> DeleteDirectoryAsync(string bucketName, string keyName);
        Task<List<S3Bucket>> ListBuckets();
        Task<List<S3Object>> ListObjectsAsync(string bucketName);

        Task<bool> CreateDirectoryAsync(string bucketName, string dirName);
        Task<bool> CreateDirectoryAsync(string bucketName, string parentDirKey, string dirName);
        Task<List<string>> ListDirectoryKeysAsync(string bucketName);
        Task<List<string>> ListDirectoryKeysAsync(string bucketName, string directoryKey);

        Task<bool> CopyObjectAsync(string sourceBucket, string objectKey, string destinationBucket, string destObjectKey);
        Task DownloadFileAsync(string targetFilePath, string bucketName, string sourceKeyName);
        Task UploadFileAsync(string filePath, string bucketName, string keyName);

        Task DownloadDirectoryAsync(String bucketName, String s3Directory, String localDirectory);
        Task DownloadDirectoryAsync(String bucketName, String s3Directory, String localDirectory, CancellationToken cancellationToken);
        Task UploadDirAsync(string sourceDirPath, string bucketName, string prefix);
        Task UploadDirAsync(string sourceDirPath, string bucketName, string prefix, CancellationToken cancellationToken);


        Task __Test();
    }
}
