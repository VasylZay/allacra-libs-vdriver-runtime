﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.RabbitMQ.Interface
{
    public interface IRabbitMQTest
    {
        Task<string> SendTestMessage();
        Task<string> SendCannedJobs();
        Task<string> SendMessagesForPerformanceTest(int messageCount);
    }
}
