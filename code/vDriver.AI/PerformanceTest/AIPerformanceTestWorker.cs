﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using RabbitMQ.Client.Events;


using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.AmazonS3.Interface;
using vDriver.DocumentLocker.Interface;
using vDriver.RabbitMQ.Interface;
using vDriver.SparkLivy.Interface;


namespace vDriver.AI
{
    public class AIPerformanceTestWorker : AIWorker
    {
        private Action<int> _threadRegisterCallback;

        public AIPerformanceTestWorker(
            IAppService appService, 
            ILogger logger,
            IAIServiceConfig config,
            IAmazonS3Service amazonS3Service,
            IDocumentLockerService documentLockerService,
            ISparkLivyService sparkLivyService,
            IRabbitMQListener rabbitMQListener,
            Func<bool, string, Task> resultCallback, 
            Action<int> workerThreadRegister) 
            : base(appService, logger, config, amazonS3Service, documentLockerService, sparkLivyService, rabbitMQListener, resultCallback)
        {
            this._threadRegisterCallback = workerThreadRegister;
        }

        public TimeSpan JobTimeSpan { get; set; }


        protected override async Task DoWork(BasicDeliverEventArgs args)
        {
            Tuple<bool, string> rslt = new Tuple<bool, string>(true, string.Empty);

            try
            {
                AITask aiTask = this.CreateAITask(args.Body);

                if (this._progressCallback != null)
                {
                    await this._progressCallback(this.CreateMessage_JobBegin(aiTask)).ConfigureAwait(false);
                }


                #region  [VZ:DEMO]

                //if (rslt.Item1)
                //{
                //    rslt = this.CreateJobFolder(aiTask);
                //}
                //if (rslt.Item1)
                //{
                //    rslt = await this.CreateS3Folder(aiTask);
                //}

                #endregion

                await this.ImmitateLongWork();


                if (this._callback != null)
                {
                    if (rslt.Item1)
                    {
                        rslt = new Tuple<bool, string>(true, this.CreateMessage_JobEnd(aiTask));
                    }

                    await this._callback(rslt.Item1, rslt.Item2).ConfigureAwait(false);
                }

                this.AcknowledgeMessageDelivery(args.DeliveryTag);
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "Failed to process message in the AIPerformanceTestWorker.DoWork(..) method.");
            }
        }


        private async Task ImmitateLongWork()
        {
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            this._threadRegisterCallback(hndl);

            await Task.Delay(JobTimeSpan);
        }

    }
}
