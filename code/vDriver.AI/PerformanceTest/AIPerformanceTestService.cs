﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;


using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.AmazonS3.Interface;
using vDriver.DocumentLocker.Interface;
using vDriver.RabbitMQ.Interface;
using vDriver.SparkLivy.Interface;


namespace vDriver.AI
{
    public class AIPerformanceTestService : AIService, IAIPerformanceTestService
    {
        private TimeSpan _jobTime;
        private int _messageCount;
        private int _processedMessageCount;

        private DateTime _dtStart;

        private readonly object _threadRegistryLock = new Object();
        private readonly HashSet<int> _threadRegistry;

        public AIPerformanceTestService(
            IAppService appService,
            ILogger<AIService> logger,
            IOptions<AIServiceConfig> config,
            IRabbitMQService rabbitMQService,
            IAmazonS3Service amazonS3Service,
            IDocumentLockerService documentLockerService,
            ISparkLivyService sparkLivyService)
        : base(appService, logger, config, rabbitMQService, amazonS3Service, documentLockerService, sparkLivyService)
        {
            this._threadRegistry = new HashSet<int>();
        }

        public void SetExpectedMessageCount(int messageCount)
        {
            this._messageCount = messageCount;
        }

        public void SetJobSimulationTime(TimeSpan time)
        {
            this._jobTime = time;
        }

        public override async Task<string> Run(IRabbitMQResilientConnection connection)
        {
            string msg = String.Format("Started processing using AIPerformanceTestService. Expected message count: {0}", this._messageCount);
            this.Logger.LogInformation(msg);

            this._dtStart = DateTime.Now;

            string rslt = await base.Run(connection);

            return rslt;
        }

        protected override AIWorker CreateWorker(IRabbitMQResilientConnection connection, int id)
        {
            string queueName = this.AIServiceConfig.AI_RabbitMQ_QueueName;
            IRabbitMQListener listener = this.RabbitMQService.CreateRabbitMQListener(connection, queueName);

            AIPerformanceTestWorker worker = new AIPerformanceTestWorker(
                this.AppService,
                this.Logger,
                this.AIServiceConfig,
                this.AmazonS3Service,
                this.DocumentLockerService,
                this.SparkLivyService,
                listener,
                this.WorkerCallback,
                this.RegisterWorkerThread)
            {
                Id = id,
                JobTimeSpan = _jobTime
            };

            return worker;
        }

        protected void RegisterWorkerThread(int id)
        {
            lock (this._threadRegistryLock)
            {
                this._threadRegistry.Add(id);
            }
        }


        protected override async Task WorkerCallback(bool successFlag, string message)
        {
            int cnt = Interlocked.Increment(ref this._processedMessageCount);

            // [VZ:DEMO]
            string msg = String.Format("Number of messages processed: {0}", cnt);
            this.Logger.LogInformation(msg + Environment.NewLine);

            if (cnt >= this._messageCount)
            {
                TimeSpan ts = DateTime.Now.Subtract(this._dtStart);

                string msgFinal = String.Format("Processed {0} test job requests in {1} sec.", cnt, ts.TotalSeconds);
                string msgCpuTime = String.Format("Allocated time for a job: {0} sec. with total CPU time: {1} sec", _jobTime.TotalSeconds, cnt * _jobTime.TotalSeconds);
                msgFinal = Environment.NewLine + msgFinal + Environment.NewLine + msgCpuTime;

                if (this._messageCount > 0 && this.WorkerCount > 0)
                {
                    double avg = (double)ts.TotalMilliseconds / cnt;
                    string msgAverage = String.Format("Average processing time per job: {0} ms", avg);
                    msgFinal = msgFinal + Environment.NewLine + msgAverage;

                    double realCpuTime = ts.TotalSeconds * this.WorkerCount;
                    double expectedCpuTime = _jobTime.TotalSeconds * this._messageCount;
                    double overhead = realCpuTime - expectedCpuTime;
                    string msgOverhead = String.Format("Parallelization overhead: {0} sec per worker per job", overhead / (cnt * this.WorkerCount));
                    msgFinal = msgFinal + Environment.NewLine + msgOverhead;
                }

                this.Logger.LogInformation(msgFinal + Environment.NewLine);

                int threadsCount = 0;
                lock (this._threadRegistryLock)
                {
                    threadsCount = this._threadRegistry.Count;
                }

                string msgThreads = String.Format("Number of threads used: {0}. Number of workers: {1}", threadsCount, this.WorkerCount);
                this.Logger.LogInformation(msgThreads + Environment.NewLine);

                //this.DumpThreadPoolInfo();
            }
        }

    }
}