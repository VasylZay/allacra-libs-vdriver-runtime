﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace vDriver.RabbitMQ.Interface
{
    public interface IRabbitMQService
    {
        IRabbitMQConfig RabbitMQConfig { get; }
        Task<IRabbitMQResilientConnection> GetConnectionAsync();
        IRabbitMQListener CreateRabbitMQListener(IRabbitMQResilientConnection connection, string queueName);
    }
}
