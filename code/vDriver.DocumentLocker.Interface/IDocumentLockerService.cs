﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.DocumentLocker.Interface
{
    public interface IDocumentLockerService
    {
        Task<Tuple<bool, string>> RequestDocuments(string s3CommandLocation, string destinationLocation, Guid rollupId);
        Task<Tuple<bool, string>> RequestDocumentsExperimental(string s3CommandLocation, string destinationLocation);
        Task<string> CallLambdaExperimental();
    }
}
