﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using Microsoft.Extensions.Logging;

using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Util;



namespace vDriver.AmazonS3
{
    public static class AmazonS3ClientExtensions
    {
        static AmazonS3ClientExtensions()
        {
            Logger = vDriver.Library.AppLogging.CreateLogger(typeof(AmazonS3ClientExtensions).FullName);
        }

        public static ILogger Logger { get; set; }

        private static int ListObjects_MaxKeys = 1000;

        public static async Task<HttpStatusCode> BlockPublicAccess(this IAmazonS3 s3Client, string bucketName)
        {
            PublicAccessBlockConfiguration conf = new PublicAccessBlockConfiguration
            {
                BlockPublicAcls = true,
                BlockPublicPolicy = true,
                IgnorePublicAcls = true,
                RestrictPublicBuckets = true
            };

            PutPublicAccessBlockRequest rqst = new PutPublicAccessBlockRequest
            {
                BucketName = bucketName,
                PublicAccessBlockConfiguration = conf
            };

            PutPublicAccessBlockResponse rsps = await s3Client.PutPublicAccessBlockAsync(rqst);

            return rsps.HttpStatusCode;
        }

        public static async Task<bool> DoesBucketExist(this IAmazonS3 s3Client, string bucketName)
        {
            return await AmazonS3Util.DoesS3BucketExistAsync(s3Client, bucketName);
        }

        public static async Task<List<S3Bucket>> GetBucketList(this IAmazonS3 s3Client)
        {
            ListBucketsResponse rsps = await s3Client.ListBucketsAsync();
            if (rsps.HttpStatusCode == HttpStatusCode.OK)
            {
                return rsps.Buckets;
            }
            else
            {
                return new List<S3Bucket>();
            }
        }

        public static async Task<List<S3Object>> ListObjectsAsync(this IAmazonS3 s3Client, string bucketName)
        {
            List<S3Object> list = new List<S3Object>();

            try
            {
                ListObjectsV2Request request = new ListObjectsV2Request
                {
                    BucketName = bucketName,
                    MaxKeys = ListObjects_MaxKeys
                };

                ListObjectsV2Response response;
                do
                {
                    response = await s3Client.ListObjectsV2Async(request);

                    // Process the response.
                    foreach (S3Object entry in response.S3Objects)
                    {
                        list.Add(entry);
                    }
                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch(Exception ex)
            {
                Logger.LogError(ex, "Failed to list S3 objects.");
            }

            return list;
        }

        // [VZ:] This should be an extension method, but I need biased 'Either' or at least 'Option'to code it right:
        // [VZ:] It takes about 30 sec for the AmazonS3Util.DoesS3BucketExistAsync(..) to return true.
        public static async Task<bool> WaitForBucketToBeAvailable(this IAmazonS3 s3Client, string bucketName, int secondsToWait = 100)
        {
            bool checkExists = false;

            int i = 0;
            while (i < secondsToWait && !checkExists)
            {
                checkExists = await AmazonS3Util.DoesS3BucketExistAsync(s3Client, bucketName);
                await Task.Delay(1000);

                i++;
            }

            return checkExists;
        }

        public static bool CreateDirectory(this IAmazonS3 s3Client, string bucketName, string dirName)
        {
            bool rslt = false;

            try
            {
                S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, bucketName);
                if (dirInfo.Exists)
                {
                    S3DirectoryInfo subdirInfo = dirInfo.GetDirectory(dirName);
                    if (!subdirInfo.Exists)
                    {
                        subdirInfo = dirInfo.CreateSubdirectory(dirName);
                    }

                    rslt = subdirInfo.Exists;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Failed to create S3 directory {0} in {1} bucket.", dirName, bucketName);
                Logger.LogError(ex, msg);
            }

            return rslt;
        }

        public static bool CreateDirectory(this IAmazonS3 s3Client, string bucketName, string parentDirKey, string dirName)
        {
            bool rslt = false;

            try
            {
                S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, parentDirKey);
                if (dirInfo.Exists)
                {
                    S3DirectoryInfo subdirInfo = dirInfo.GetDirectory(dirName);
                    if (!subdirInfo.Exists)
                    {
                        subdirInfo = dirInfo.CreateSubdirectory(dirName);
                    }

                    rslt = subdirInfo.Exists;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Failed to create S3 sub-directory {0} inside {1} directory in {2} bucket.", dirName, parentDirKey, bucketName);
                Logger.LogError(ex, msg);
            }

            return rslt;
        }

        public static bool DeleteDirectory(this IAmazonS3 s3Client, string bucketName, string dirKey)
        {
            bool rslt = false;

            try
            {
                S3DirectoryInfo dirInfo = new S3DirectoryInfo(s3Client, bucketName, dirKey);
                if (dirInfo.Exists)
                {
                    dirInfo.Delete(true);
                    rslt = ! dirInfo.Exists;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("Failed to delete S3 directory {0} in {1} bucket.", dirKey, bucketName);
                Logger.LogError(ex, msg);
            }

            return rslt;
        }

    }
}