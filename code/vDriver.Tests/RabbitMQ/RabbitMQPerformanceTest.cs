﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using RabbitMQ.Client;

using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.RabbitMQ.Interface;


namespace vDriver.Tests
{
    public class RabbitMQPerformanceTest : RabbitMQTest
    {
        public RabbitMQPerformanceTest(IAppService appService, ILogger<RabbitMQTest> logger, IRabbitMQService mqService) 
            : base(appService, logger, mqService)
        {
        }

    }
}
