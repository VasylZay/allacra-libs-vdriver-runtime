﻿namespace vDriver.SparkLivy
{
    // [VZ:] According to https://livy.incubator.apache.org/docs/latest/rest-api.html
    // If session kind is not specified or the submitted code is not the kind specified in session creation, 
    // this field should be filled with correct kind. Otherwise Livy will use kind specified in session creation 
    // as the default code kind.
    // [VZ:] Hence either specify the session kind or specify code kind with every submitted statement.
    public enum SparkCodeKind
    {
        Spark = 1,
        PySpark = 2,
        SparkR = 3,
        SQL = 4
    }

}
