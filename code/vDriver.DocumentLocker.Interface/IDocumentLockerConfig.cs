﻿namespace vDriver.DocumentLocker.Interface
{
    public interface IDocumentLockerConfig
    {
        string AWSRegion { get; }
        string AWSCredentialProfileName { get; }
    }
}
