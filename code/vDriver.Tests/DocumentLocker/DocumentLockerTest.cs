﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;

using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.AmazonS3.Interface;
using vDriver.DocumentLocker.Interface;



namespace vDriver.Tests
{
    public class DocumentLockerTest : IDocumentLockerTest
    {
        public DocumentLockerTest(IAppService appService, ILogger<DocumentLockerTest> logger, IAIService aiService, IAmazonS3Service amazonS3Service, IDocumentLockerService srv)
        {
            this.AppService = appService;
            this.Logger = logger;
            this.AIService = aiService;
            this.AmazonS3Service = amazonS3Service;
            this.DocumentLockerService = srv;
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }
        public IAIService AIService { get; private set; }
        public IDocumentLockerService DocumentLockerService { get; private set; }
        public IAmazonS3Service AmazonS3Service { get; private set; }

        public async Task<string> Run()
        {
            string s3CommandLocation = "";
            string destinationLocation = "";


            // [1] [VZ:] This is to test different ways to deal with long running remote calls:
            //Tuple<bool, string> rslt = await DocumentLockerService.RequestDocumentsExperimental(s3CommandLocation, destinationLocation);

            // [2] [VZ:] This is to simulate Document Locker functionality using canned datasets for AI Job
            Guid rollupId = new Guid("7077154c-9f4d-4daf-8d05-93ee6c4a9c90");
            destinationLocation = await this.GetS3DestinationForCannedJob(rollupId);

            Stopwatch stopwatch = Stopwatch.StartNew();
            Tuple<bool, string> rslt = await DocumentLockerService.RequestDocuments(s3CommandLocation, destinationLocation, rollupId);
            stopwatch.Stop();

            if (rslt.Item1)
            {
                string msg = "Documents are ready and written to: " + Environment.NewLine + rslt.Item2 + Environment.NewLine +
                    "Time spent: " + stopwatch.ElapsedMilliseconds + " ms";
                this.Logger.LogInformation(msg);
            }
            else
            {
                string msg = "There was an error requesting documents. ";
                if(!string.IsNullOrEmpty(rslt.Item2))
                {
                    msg = msg + Environment.NewLine + rslt.Item2;
                }

                this.Logger.LogError(msg);
            }

            return rslt.Item2;
        }

        private async Task<string> GetS3DestinationForCannedJob(Guid rollupId)
        {
            string destinationLocation = null;
            try
            {
                IAIServiceConfig config = this.AIService.AIServiceConfig;
                string backetName = config.AIS3BaseBucket;
                string folderPath = config.AIS3WorkingFolder;

                destinationLocation = await this.AmazonS3Service.FormatAndVerifyS3Path(backetName, folderPath, rollupId.ToString() + "/data");
            }
            catch (Exception ex)
            {
                string msg = string.Format("Failed to get destination location for canned job with rollupId = {0} \nEXCEPTION: {1}", rollupId, ex.Message);
                this.Logger.LogError(msg);
            }

            return destinationLocation;
        }
    }
}
