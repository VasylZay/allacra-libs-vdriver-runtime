﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using System.IO;
using Xunit;
using vDriver;


namespace vDriver.IntegrationTests
{
    public class SimpleTest
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void PassingTest()
        {
            // [VZ:REVISIT]
            // [VZ:DEBUG] Making sure configuration settings are reachable, cause there is an issue with XUnit (and NUnit too):
            // https://stackoverflow.com/questions/52797/how-do-i-get-the-path-of-the-assembly-the-code-is-in
            // string url = vDriverConfig.GetLivySparkClusterUrl();

            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void FailingTest()
        {
            Assert.Equal(5, Add(2, 2));
        }

        int Add(int x, int y)
        {
            return x + y;
        }
    }
}
