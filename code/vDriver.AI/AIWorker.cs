﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client.Events;

using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.AmazonS3.Interface;
using vDriver.DocumentLocker.Interface;
using vDriver.RabbitMQ.Interface;
using vDriver.SparkLivy.Interface;


namespace vDriver.AI
{
    public partial class AIWorker 
    {
        public static string AboutFileName = "about.json";
        public static string CombatchFileName = "compbatch.json";
        public static string DataSubdirectory = "data";
        public static string ResultsSubdirectory = "results";


        public static Encoding MessageEncoding = Encoding.UTF8;

        protected readonly Func<bool, string, Task> _callback;
        protected readonly Func<string, Task> _progressCallback;

        public AIWorker(
            IAppService appService,
            ILogger logger,
            IAIServiceConfig config,
            IAmazonS3Service amazonS3Service,
            IDocumentLockerService documentLockerService,
            ISparkLivyService sparkLivyService,
            IRabbitMQListener rabbitMQListener,
            Func<bool, string, Task> resultCallback = null,
            Func<string, Task> progressCallback = null)
            : this(rabbitMQListener)
        {
            this.AppService = appService;
            this.Logger = logger;
            this.AIServiceConfig = config;
            this.AmazonS3Service = amazonS3Service;
            this.DocumentLockerService = documentLockerService;
            this.SparkLivyService = sparkLivyService;
            this._callback = resultCallback;
            this._progressCallback = progressCallback;
        }


        public IAppService AppService { get; private set; }

        public IAIServiceConfig AIServiceConfig { get; private set; }

        public IAmazonS3Service AmazonS3Service { get; private set; }

        public ISparkLivyService SparkLivyService { get; private set; }

        public IDocumentLockerService DocumentLockerService { get; private set; }

        public ILogger Logger { get; private set; }

        protected virtual async Task DoWork(BasicDeliverEventArgs args)
        {
            Tuple<bool, string> rslt = new Tuple<bool, string>(true, string.Empty);

            try
            {
                AITask aiTask = this.CreateAITask(args.Body);

                if (this._progressCallback != null)
                {
                    await Task.Run(() => this._progressCallback(this.CreateMessage_JobBegin(aiTask)).ConfigureAwait(false));
                }

                if (rslt.Item1) 
                {
                    rslt = this.CreateJobFolder(aiTask);
                }


                if (rslt.Item1)
                {
                    rslt = await this.CreateS3Folder(aiTask).ConfigureAwait(false);
                }

                // [VZ:DEVTIME] [!!!] Comment this out for dev time to avoid accessive S3 hitting
                //if (rslt.Item1)
                //{
                //    rslt = await this.RequestDocuments(aiTask).ConfigureAwait(false);
                //}

                if (rslt.Item1)
                {
                    rslt = await this.RunSparkBatchJob(aiTask).ConfigureAwait(false);
                }

                // [VZ:CRITICAL]
                //if (rslt.Item1)
                {
                    this.AcknowledgeMessageDelivery(args.DeliveryTag);
                }

                if (this._callback != null)
                {
                    if (rslt.Item1)
                    {
                        rslt = new Tuple<bool, string>(true, this.CreateMessage_JobEnd(aiTask));
                    }

                    await this._callback(rslt.Item1, rslt.Item2).ConfigureAwait(false);
                }
            }
            catch(Exception e)
            {
                this.Logger.LogError(e, "Exception in AIWorker.DoWork");
            }
        }

        protected void CancelWork(string reason)
        {
            string msg = string.Format("Booked job was cancelled. Reason: " + reason);
            this.Logger.LogWarning(msg);
        }

        protected Tuple<bool, string> CreateJobFolder(AITask aiTask)
        {
            bool flag = false;
            string message = string.Empty;

            try
            {
                string folderPath = Path.Combine(this.AIServiceConfig.AIJobsFolderPath, aiTask.JobId.ToString());
                DirectoryInfo dirInfo = Directory.CreateDirectory(folderPath);
                if (dirInfo.Exists)
                {
                    string filePath = Path.Combine(dirInfo.FullName, CombatchFileName);
                    File.WriteAllText(filePath, aiTask.MessagePayload, MessageEncoding);

                    string filePathAbout = Path.Combine(dirInfo.FullName, AboutFileName);
                    string payload = JsonConvert.SerializeObject(aiTask);
                    File.WriteAllText(filePathAbout, payload, MessageEncoding);

                    flag = dirInfo.CreateSubdirectory(AIWorker.DataSubdirectory).Exists;
                    flag = dirInfo.CreateSubdirectory(AIWorker.ResultsSubdirectory).Exists;

                    aiTask.LocalDirPath = folderPath;
                }
            }
            catch (Exception e)
            {
                message = "Failed to create job folder.";
                this.Logger.LogError(e, message);
            }

            return new Tuple<bool, string> (flag, message);
        }

        protected async Task<Tuple<bool, string>> CreateS3Folder(AITask aiTask)
        {
            bool flag = false;
            string message = string.Empty;

            try
            {
                string backetName = this.AIServiceConfig.AIS3BaseBucket;
                string workingFolder = this.AIServiceConfig.AIS3WorkingFolder;
                string jobFolder = Path.Combine(workingFolder, aiTask.JobId.ToString());
                string dataSubFolder = Path.Combine(aiTask.JobId.ToString(), "data");
                string resultsSubFolder = Path.Combine(aiTask.JobId.ToString(), "results");

                string s3CommandPath = await this.AmazonS3Service.FormatAndVerifyS3Path(backetName, workingFolder, aiTask.JobId.ToString());
                string s3DataPath = await this.AmazonS3Service.FormatAndVerifyS3Path(backetName, workingFolder, dataSubFolder);
                string s3ResultsPath = await this.AmazonS3Service.FormatAndVerifyS3Path(backetName, workingFolder, resultsSubFolder);

                // [VZ:] We can certainly go crazy with functional style, but it is more efficient to store the context:
                aiTask.BacketName = backetName;
                aiTask.AIWorkingFolder = workingFolder;
                aiTask.JobFolder = jobFolder;
                aiTask.DataSubfolder = dataSubFolder;
                aiTask.ResultsSubfolder = resultsSubFolder;
                aiTask.S3CommandPath = s3CommandPath;
                aiTask.S3DataPath = s3DataPath;
                aiTask.S3ResultsPath = s3ResultsPath;

                await this.AmazonS3Service.UploadDirAsync(aiTask.LocalDirPath, backetName, jobFolder);

                flag = true;
            }
            catch(Exception e)
            {
                message = "Failed to create Amazon S3 job folder.";
                this.Logger.LogError(e, message);
            }

            return new Tuple<bool, string>(flag, message);
        }

        protected async Task<Tuple<bool, string>> RequestDocuments(AITask aiTask)
        {
            Tuple<bool, string> rslt = new Tuple<bool, string>(false, string.Empty);

            try
            {

                string s3CommandPath = aiTask.S3CommandPath;
                string s3DataPath = aiTask.S3DataPath;
                rslt = await this.DocumentLockerService.RequestDocuments(s3CommandPath, s3DataPath, aiTask.RollupId);
            }
            catch (Exception e)
            {
                string message = String.Format("Failed to request documents from Document Locker. \nEXCEPTION: {0}", e.Message);
                this.Logger.LogError(e, message);
                rslt = new Tuple<bool, string>(false, message);
            }

            return rslt;
        }

        protected async Task<Tuple<bool, string>> RunSparkBatchJob(AITask aiTask)
        {
            bool flag = false;
            string message = string.Empty;

            ISparkLivyConfig config = this.SparkLivyService.SparkLivyConfig;

            JObject dataObject = new JObject
            {

                // [VZ:] Path to the Scala Spark .jar file:
                { "file", config.ClassifierJarS3Path }, // "s3://vz-bucket/jars/classifier_2.11-1.0.jar"

                // [VZ:] Scala Classname with Main(..)
                { "className", config.ClassifierClassName } // "com.alacra.spark.ClassifierApp"

                // [VZ:REVISIT] Looks like the following has no effect:
                //dataObject.Add("name", "Vasyl's name of this batch job");
            };


            // [VZ:] Path to the pre-trained AI model:
            string modelPath = config.ClassifierModelS3Path; // "s3://factiva-tprm/model_1.0"

            string dataPath = aiTask.S3DataPath;
            string resultsPath = aiTask.S3ResultsPath;
            JArray argsObject = new JArray(new string[] { modelPath, dataPath, resultsPath });

            dataObject.Add("args", argsObject);

            ILivyBatchJob job = this.SparkLivyService.CreateSparkLivyBacthJob();

            //ILivyBatchJobResult result = await job.ExecuteBatchJobAsync(dataObject);
            message = "Need Scala Spark .jar file to finish AI job!";

            return new Tuple<bool, string>(flag, message);
        }


        private AIRequestMessageEnvelope ParseMessageLight(byte[] payload)
        {
            AIRequestMessageEnvelope message = null;

            try
            {
                string jsonPayload = Encoding.UTF8.GetString(payload);
                message = JsonConvert.DeserializeObject<AIRequestMessageEnvelope>(jsonPayload);
            }
            catch(Exception ex)
            {
                string msg = "Failed to parse RabbitMQ message.";
                this.Logger.LogError(ex, msg);
            }

            return message;
        }

        protected AITask CreateAITask(byte[] payload)
        {
            AITask task = null;

            try
            {
                string jsonPayload = Encoding.UTF8.GetString(payload);
                AIRequestMessageEnvelope msgEnvelope = JsonConvert.DeserializeObject<AIRequestMessageEnvelope>(jsonPayload);
                Guid messageId = msgEnvelope.message_id;
                DateTime timeStamp = msgEnvelope.timestamp;
                Guid rollupId = msgEnvelope.request_rollup_batch.id;
                string serviceId = msgEnvelope.request_rollup_batch.service_id;

                Guid jobId = Guid.NewGuid();

                task = new AITask(jobId, jsonPayload, messageId, timeStamp, rollupId, serviceId);
            }
            catch (Exception e)
            {
                string msg = "Failed to parse RabbitMQ message. \nEXCEPTION: " + e.Message;
                this.Logger.LogError(e, msg);
            }

            return task;
        }

        protected virtual string CreateMessage_JobBegin(AITask aITask)
        {
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string msgAttribs = String.Format("Message Id = {0} \nRollup Id = {1}", aITask.MessageId, aITask.RollupId);
            string msg = String.Format("Started processing RabbitMQ message: \n{0} \nWorker = {1} | Thread = {2}  \n", msgAttribs, this.Id.ToString(), hndl.ToString());

            return msg;
        }

        protected virtual string CreateMessage_JobEnd(AITask aITask)
        {
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string msgAttribs = String.Format("Message Id = {0} \nRollup Id = {1}", aITask.MessageId, aITask.RollupId);
            string msg = String.Format("Finished processing RabbitMQ message: \n{0} \nWorker = {1} | Thread = {2} ", msgAttribs, this.Id.ToString(), hndl.ToString());

            return msg;
        }

    }



}
