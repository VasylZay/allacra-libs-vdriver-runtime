﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using vDriver.Interface;
using vDriver.AmazonS3.Interface;

namespace vDriver.DocumentLocker
{
    // [VZ:] This worker will simulate Document Locker functionality by copying files 
    // for AI processing from specified s3 directory
    public class DocumentLockerWorkerForCannedJob
    {
        public DocumentLockerWorkerForCannedJob(IAppService appService, ILogger logger, IAmazonS3Service amazonS3Service)
        {
            this.AppService = appService;
            this.Logger = logger;
            this.AmazonS3Service = amazonS3Service;
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }
        public IAmazonS3Service AmazonS3Service { get; private set; }

        public async Task<Tuple<bool, string>> RequestDocumentsAsync(string s3SourceLocation, string destinationLocation)
        {
            // [VZ:CLEANUP]
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string infoMsg = string.Format("DocumentLocker worker is running on the managed thread, id = {0}", hndl);
            this.Logger.LogInformation(infoMsg);

            bool successFlag = false;
            string message = string.Empty;

            try
            {
                Uri sourceUri = new Uri(s3SourceLocation);
                string sourceBucket = sourceUri.Authority;
                string sourcePrefix = sourceUri.AbsolutePath;
                string localPath = sourceUri.LocalPath;


                Uri destinationUri = new Uri(destinationLocation);
                string destinationBucket = destinationUri.Authority ;
                string destinationPrefix = destinationUri.AbsolutePath + localPath;

                successFlag = await this.AmazonS3Service.CopyObjectAsync(sourceBucket, sourcePrefix, destinationBucket, destinationPrefix).ConfigureAwait(false);
                message = destinationUri.ToString();
            }
            catch (Exception ex)
            {
                message = "Failed to move documents in the DocumentLockerWorkerForCannedJob.";
                this.Logger.LogError(ex, message);
            }


            return new Tuple<bool, string>(successFlag, message);
        }
    }
}