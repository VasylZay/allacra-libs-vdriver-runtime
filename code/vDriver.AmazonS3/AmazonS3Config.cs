﻿using vDriver.AmazonS3.Interface;

namespace vDriver.AmazonS3
{
    public class AmazonS3Config : IAmazonS3Config
    {
        public string AWSRegion { get; set; }
        public string AWSCredentialProfileName { get; set; }
        //public string AWSCredentialProfileLocation { get; set; }
        //public string AWSAccessKey { get; set; }
        //public string AWSSecretKey { get; set; }
    }
}
