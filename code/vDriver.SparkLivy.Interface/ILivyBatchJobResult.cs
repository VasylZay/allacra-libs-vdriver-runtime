﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace vDriver.SparkLivy.Interface
{
    public interface ILivyBatchJobResult
    {
        bool SuccessFlag { get; set; } // default false;
        string Message { get; set; } // default "Batch job did not started yet";
        JObject Result { get; set; }  
    }
}
