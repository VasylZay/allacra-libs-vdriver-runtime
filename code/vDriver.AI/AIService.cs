﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using vDriver.Interface;
using vDriver.AI.Interface;
using vDriver.AmazonS3.Interface;
using vDriver.DocumentLocker.Interface;
using vDriver.RabbitMQ.Interface;
using vDriver.SparkLivy.Interface;


namespace vDriver.AI
{

    public class AIService : IAIService
    {
        private readonly List<AIWorker> _workers;

        private bool _isS3Initialized;

        private int _messageCountSuccess;
        private int _messageCountFailed;


        public AIService(
            IAppService appService,
            ILogger<AIService> logger,
            IOptions<AIServiceConfig> config,
            IRabbitMQService rabbitMQService,
            IAmazonS3Service amazonS3Service,
            IDocumentLockerService documentLockerService,
            ISparkLivyService sparkLivyService
            )
        {
            this.AppService = appService;
            this.Logger = logger;
            this.AIServiceConfig = config.Value;
            this.AmazonS3Service = amazonS3Service;
            this.RabbitMQService = rabbitMQService;
            this.DocumentLockerService = documentLockerService;
            this.SparkLivyService = sparkLivyService;

            this.WorkerCount = this.AIServiceConfig.AIWorkersCount;
            this._workers = new List<AIWorker>(this.WorkerCount);

            this.AppService.ShutdownApplication += Application_Shutdown;
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }
        public IAIServiceConfig AIServiceConfig { get; private set; }
        public IAmazonS3Service AmazonS3Service { get; private set; }
        public IDocumentLockerService DocumentLockerService { get; private set; }
        public IRabbitMQService RabbitMQService { get; private set; }
        public ISparkLivyService SparkLivyService { get; private set; }

        public int WorkerCount { get; private set; }

        private IRabbitMQResilientConnection _connection;

        public virtual async Task<string> Run()
        {
            string rslt = string.Empty;

            if (!this._isS3Initialized)
            {
                this._isS3Initialized = await this.VerifyS3BucketPath();
            }

            if (this._connection == null)
            {
                this._connection = await this.RabbitMQService.GetConnectionAsync();
                this._connection.ShutdownConnection += Connection_ShutdownConnection;
            }

            if (!this._connection.IsConnected)
            {
                await this._connection.InitializeAsync();
            }

            rslt = await this.Run(this._connection);

            return rslt;
        }

        private async void Connection_ShutdownConnection(object sender, EventArgs e)
        {
            await this.Run();
        }

        public virtual async Task<string> Run(IRabbitMQResilientConnection connection)
        {
            string rslt = string.Empty;

            foreach (AIWorker wrkr in this._workers)
            {
                wrkr.Dispose();
            }

            this._workers.Clear();

            for (int i = 0; i < this.WorkerCount; i++)
            {
                this._workers.Add(this.CreateWorker(connection, i));
            }

            foreach (AIWorker wrkr in this._workers)
            {
                await Task.Run(() => wrkr.Run());
            }

            return rslt;
        }

        private async Task<bool> VerifyS3BucketPath()
        {
            bool rslt = await this.AmazonS3Service.ConnectAsync();

            if (rslt)
            {
                string bucketName = this.AIServiceConfig.AIS3BaseBucket;
                bool doesBucketExist = await this.AmazonS3Service.DoesBucketExist(bucketName);
                if (!doesBucketExist)
                {
                    rslt = await this.AmazonS3Service.CreateBucketAsync(bucketName);
                }

                if (rslt)
                {
                    string folderName = this.AIServiceConfig.AIS3WorkingFolder;
                    rslt = await this.AmazonS3Service.CreateDirectoryAsync(bucketName, folderName);
                }
            }

            return rslt;
        }

        protected virtual AIWorker CreateWorker(IRabbitMQResilientConnection connection, int id)
        {
            string queueName = this.AIServiceConfig.AI_RabbitMQ_QueueName;
            IRabbitMQListener listener = this.RabbitMQService.CreateRabbitMQListener(connection, queueName);

            AIWorker worker = new AIWorker(
                this.AppService, 
                this.Logger, 
                this.AIServiceConfig, 
                this.AmazonS3Service, 
                this.DocumentLockerService, 
                this.SparkLivyService, 
                listener, 
                this.WorkerCallback, 
                this.ProgressCallBack);

            worker.Id = id;

            return worker;
        }

        #region callbacks

        // [VZ:REVISIT] Not sure we need to keep it async
        protected virtual async Task ProgressCallBack(string message)
        {
            this.Logger.LogInformation(message);
        }

        // [VZ:REVISIT] Not sure we need to keep it async
        protected virtual async Task WorkerCallback(bool successFlag, string message)
        {
            if (successFlag)
            {
                Interlocked.Increment(ref this._messageCountSuccess);
            }
            else
            {
                Interlocked.Increment(ref this._messageCountFailed);
            }

            //string msg = String.Format("Number of messages processed: {0}", cnt);
            //this.Logger.LogInformation(msg + Environment.NewLine);

            this.Logger.LogInformation(message + Environment.NewLine);

            return;
        }


        private void Application_Shutdown(object sender, EventArgs e)
        {
            foreach (AIWorker wrkr in this._workers)
            {
                wrkr.Dispose();
            }
        }

        #endregion

    }
}
