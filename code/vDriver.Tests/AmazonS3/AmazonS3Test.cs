﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using vDriver.Interface;
using vDriver.AmazonS3.Interface;

namespace vDriver.Tests
{
    public class AmazonS3Test : IAmazonS3Test
    {

        public AmazonS3Test(IAppService appService, ILogger<AmazonS3Test> logger, IAmazonS3Service srv)
        {
            this.AppService = appService;
            this.Logger = logger;
            this.AmazonS3Service = srv;
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }
        public IAmazonS3Service AmazonS3Service { get; private set; }


        public async Task<string> Run()
        {
            string rslt = string.Empty;

            bool isConnected = await AmazonS3Service.ConnectAsync();
            if (isConnected)
            {
                //await this.CopyS3ObjectTest_1(); // [!] 5.5 MB file - careful!
                await this.CopyS3ObjectTest_2(); 


                /*
                string bucketName = "vz-bucket-test-east-2";

                //await AmazonS3Service.Test();

                // Test Upload-Download
                string fileName = @"C:\AI\Spark.NET\vDriver\vDriver\vz-readme.txt";
                string keyName = "Kkk/Qqq/my-readme.txt";
                await AmazonS3Service.UploadFileAsync(fileName, bucketName, keyName);
                //await AmazonS3Service.DownloadFileAsync("zzz.txt", bucketName, keyName);


                IServiceProvider srvs = this.AppService.GetServiceProvider();
                IAppConfig appConfig = srvs.GetService<IAppConfig>();
                string jobName = "ai-job-12345";
                string jobsDirPath = Path.Combine(appConfig.AIJobsFolderPath, jobName);
                string prefix = String.Format("ai-jobs/{0}", jobName);
                //await AmazonS3Service.UploadDirAsync(jobsDirPath, bucketName, prefix);

                //string s3Directory = "s3://vz-bucket-test-east-2/ai-jobs/ai-job-12345";
                string s3Directory = prefix; // "ai-jobs/ai-job-12345";
                string localDirectory = Path.Combine(@"C:\Temp\Zzz", jobName);
                await AmazonS3Service.DownloadDirectoryAsync(bucketName, s3Directory, localDirectory);



                //List<string> dirList = await AmazonS3Service.ListingDirectoryKeysAsync(bucketName);
                //List<string> dirList = await AmazonS3Service.ListingDirectoryKeysAsync(bucketName, @"Zzz\");
                //List<string> dirList = await AmazonS3Service.ListingDirectoryKeysAsync(bucketName, "Zzz");

                var objectList = await AmazonS3Service.ListingObjectsAsync(bucketName);


                //bool isCreated = await AmazonS3Service.CreateDirectoryAsync(bucketName, dirKey);
                //bool isDeleted = await AmazonS3Service.DeleteDirectoryAsync(bucketName, dirKey);

                //bool isDeleted = await AmazonS3Service.DeleteObjectAsync(bucketName, "Zzz/Aaa");




                //bool isDeleted = await AmazonS3Service.DeleteBucketAsync(bucketName);

                // [VZ:] If bucket already exists the following will quickly return: isCreated = false; doesExist = true;
                //bool isCreated = await AmazonS3Service.CreateBucketAsync(bucketName);
                //bool doesExist = await AmazonS3Service.DoesBucketExist(bucketName);
                */
            }

            return rslt;
        }

        private async Task CopyS3ObjectTest_1()
        {
            // s3://factiva-tprm/20170921.txt.gz
            // s3://factiva-tprm/20171008.txt.gz - 5.5 MB
            string sourceBucket = "factiva-tprm";
            string objectKey = "20171008.txt.gz";
            string destinationBucket = "vz-bucket-test-east-2";
            string destObjectKey = objectKey;

            await AmazonS3Service.CopyObjectAsync(sourceBucket, objectKey, destinationBucket, destObjectKey);
        }

        private async Task CopyS3ObjectTest_2()
        {
            // [VZ:] This should copy small "about.txt" file from one S3 directory to another
            // You cannot copy directories: they are not objects:
            //s3://vz-bucket-test-east-2/ai-jobs/ai-job-12345/about.txt

            string sourceBucket = "vz-bucket-test-east-2";
            string objectKey = "ai-jobs/ai-job-12345/about.txt";
            string destinationBucket = "vz-bucket-test-east-2";
            string destObjectKey = "AIJobs/ai-job-12345/about.txt";


            await AmazonS3Service.CopyObjectAsync(sourceBucket, objectKey, destinationBucket, destObjectKey);
        }


    }
}
