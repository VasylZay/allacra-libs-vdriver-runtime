﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using vDriver.RabbitMQ.Interface;

using vDriver.RabbitMQ;

namespace vDriver.AI
{
    public partial class AIWorker
    {
        private readonly IRabbitMQListener _queueListener;

        public AIWorker(IRabbitMQListener queueListener)
        {
            this._queueListener = queueListener;
            this._queueListener.RegisterCallbacks(this.DoWork, this.CancelWork);
        }

        public int Id { get; set; }

        public void Run()
        {
            this._queueListener.Start();
        }

        protected virtual void AcknowledgeMessageDelivery(ulong deliveryTag)
        {
            this._queueListener.AcknowledgeMessageDelivery(deliveryTag);
        }

        #region Dispose

        ~AIWorker()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._queueListener != null)
                {
                    this._queueListener.Dispose();
                }
            }
        }

        #endregion
    }
}

