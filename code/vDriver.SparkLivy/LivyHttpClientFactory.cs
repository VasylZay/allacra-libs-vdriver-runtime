﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace vDriver.SparkLivy
{
    public static class LivyHttpClientFactory
    {
        public static HttpClient CreateClient(string url)
        {
            var clientHandler = new HttpClientHandler();
            var client = new HttpClient(clientHandler)
            {
                BaseAddress = new Uri(url.EndsWith("/") ? url : $"{url}/"),
                DefaultRequestHeaders =
                {
                    Accept = { new MediaTypeWithQualityHeaderValue("application/json") }
                }
            };

            return client;
        }

        public static HttpClient CreateClient(string url, string username, string password)
        {
            var clientHandler = new HttpClientHandler
            {
                Credentials = new NetworkCredential(username, password)
            };

            var client = new HttpClient(clientHandler)
            {
                BaseAddress = new Uri(url.EndsWith("/") ? url : $"{url}/"),
                DefaultRequestHeaders =
                {
                    Accept = { new MediaTypeWithQualityHeaderValue("application/json") }
                }
            };

            return client;
        }
    }
}
