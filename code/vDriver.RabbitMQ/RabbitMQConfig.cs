﻿using System;
using vDriver.RabbitMQ.Interface;

namespace vDriver.RabbitMQ
{
    public class RabbitMQConfig : IRabbitMQConfig
    {
        public ushort Port { get; set; } = 5672;
        public string ServerHost { get; set; } = "localhost";
        public string UserName { get; set; } = "guest";
        public string Password { get; set; } = "guest";
        public bool AutomaticRecoveryEnabled { get; set; } = true;
        public TimeSpan NetworkRecoveryInterval { get; set; } = new TimeSpan(0, 0, 5);
        public int RequestedConnectionTimeout { get; set; } = 30000;
        public ushort RequestedHeartbeat { get; set; } = 60;
    }

}
