﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.SparkLivy
{
    public static class LivySparkUtils
    {
        public static string TranslateCodeKind(SparkCodeKind kind)
        {
            string rslt = string.Empty;

            switch (kind)
            {
                case SparkCodeKind.Spark:
                    rslt = "spark";
                    break;

                case SparkCodeKind.PySpark:
                    rslt = "pyspark";
                    break;

                case SparkCodeKind.SparkR:
                    rslt = "sparkr";
                    break;

                case SparkCodeKind.SQL:
                    rslt = "sql";
                    break;

            }

            return rslt;
        }

        public static string GetExtendedErrorMessage(System.Net.Http.HttpRequestException e)
        {
            string message = e.Message;

            StringBuilder sb = new StringBuilder();
            sb.Append("EXCEPTION: " + e.Message);
            if (e.InnerException != null)
            {
                sb.Append(Environment.NewLine);
                sb.Append(e.InnerException.Message);
            }

            var data = e.Data;
            if (data.Count > 0)
            {
                sb.Append(Environment.NewLine);
                sb.Append("  Extra details:");
                foreach (System.Collections.DictionaryEntry de in data)
                {
                    sb.Append(Environment.NewLine);
                    sb.Append(String.Format("    Key: {0,-20}      Value: {1}",
                                      "'" + de.Key.ToString() + "'", de.Value));
                }
            }

            return sb.ToString();
        }
    }
}
