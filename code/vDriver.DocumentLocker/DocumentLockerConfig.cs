﻿using vDriver.DocumentLocker.Interface;

namespace vDriver.DocumentLocker
{
    public class DocumentLockerConfig : IDocumentLockerConfig
    {
        public string AWSRegion { get; set; }
        public string AWSCredentialProfileName { get; set; }
    }
}
