call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat"

nuget restore %~dp0../code/vDriver.Runtime.sln

msbuild %~dp0../code/Alacra.RabbitMQ.sln /t:Clean,Build /p:Configuration=Release

nuget pack %~dp0\Package.nuspec

nuget push *.nupkg -Source http://nexus.alacra.com/repository/alacra/  -ApiKey f9af0c42-f7a4-3b9e-ac42-226de327b000

del *.nupkg
pause