﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.AI.Interface
{
    public class AIRequestMessageEnvelope
    {
        public Guid message_id { get; set; }
        public DateTime timestamp { get; set; }
        public RequestRollupBatchEnvelope request_rollup_batch { get; set; }

        public class RequestRollupBatchEnvelope
        {
            public Guid id { get; set; }
            public string service_id { get; set; }
        }
    }
}
