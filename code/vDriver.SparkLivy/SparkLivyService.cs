﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;


using vDriver.Interface;
using vDriver.SparkLivy.Interface;

namespace vDriver.SparkLivy
{
    public class SparkLivyService : ISparkLivyService
    {

        public SparkLivyService(ILogger<SparkLivyService> logger, IOptions<SparkLivyConfig> config)
        {
            this.Logger = logger;
            this.SparkLivyConfig = config.Value;
        }

        public ILogger Logger { get; private set; }
        public ISparkLivyConfig SparkLivyConfig { get; private set; }

        public ILivyBatchJob CreateSparkLivyBacthJob()
        {
            return new LivyBatchJob(this.Logger, this.SparkLivyConfig);
        }
    }
}



