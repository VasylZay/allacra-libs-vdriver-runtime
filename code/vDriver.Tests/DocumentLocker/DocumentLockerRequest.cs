﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// [VZ:] This is was used as a sample how to send request to DocumentLocker and how DocumentLocker can process it:

namespace vDriver.DocumentLocker
{
    public class DocumentLockerRequest
    {
        public string rollup_Id { get; set; }
        public string backpack_type { get; set; }
        public string s3Path { get; set; }
    }
}

namespace DocumentLocker
{
    public class DocumentLockerRequest
    {
        public string rollup_Id { get; set; }
        public string backpack_type { get; set; }
        public string data { get; set; }
    }
}
