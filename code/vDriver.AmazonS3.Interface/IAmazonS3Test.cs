﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.AmazonS3.Interface
{
    public interface IAmazonS3Test
    {
        Task<string> Run();
    }
}
