﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.Interface
{
    public interface IAppConfig
    {
        string AppDirectory { get; }
        string TempDirectory { get; }
    }
}
