﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

using vDriver.RabbitMQ.Interface;

namespace vDriver.RabbitMQ
{
    public static class RabbitMQPersistentConnectionFactory
    {
    }

    public class RabbitMQResilientConnection : IRabbitMQResilientConnection, IDisposable
    {
        public event EventHandler ShutdownConnection;

        private readonly ILogger _logger;
        private readonly IRabbitMQConfig _config;

        // [VZ:] this is async lock object:
        private readonly SemaphoreSlim _mutex = new SemaphoreSlim(1, 1);


        private IConnection _connection;
        private bool _isDisposed;

        public RabbitMQResilientConnection(ILogger logger, IRabbitMQConfig config)
        {
            this._logger = logger;
            this._config = config;
        }


        // [VZ:] Careful with InitializeAsync: SemaphoreSlim, it's not quite the same as Monitor. 
        // Unlike Monitor it isn't reentrant: if you try to reacquire a Semaphore you already own, it will deadlock.
        public async Task InitializeAsync()
        {
            await this._mutex.WaitAsync();
            try
            {
                this._connection = await this.GetConnectionAsync();
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex, "Cannot initialize RabbitMQResilientConnection.");
            }
            finally
            {
                this._mutex.Release();
            }
        }

        private IConnection CreateConnection(IRabbitMQConfig config)
        {
            ConnectionFactory factory = new ConnectionFactory()
            {
                HostName = this._config.ServerHost,
                Port = this._config.Port,
                AutomaticRecoveryEnabled = this._config.AutomaticRecoveryEnabled,
                RequestedConnectionTimeout = this._config.RequestedConnectionTimeout,
                RequestedHeartbeat = this._config.RequestedHeartbeat,
                NetworkRecoveryInterval = this._config.NetworkRecoveryInterval
            };

            return factory.CreateConnection();
        }

        private async Task<IConnection> GetConnectionAsync()
        {
            if (!this.IsConnected)
            {
                if (this._connection != null) { this._connection.Dispose(); }

                // [VZ:DEMO]
                //IConnection conn = await this.GetConnectionAsync_WaitAndRetry();
                IConnection conn = await this.GetConnectionAsync_WaitAndRetryForever();

                if (conn != null && conn.IsOpen)
                {
                    this._connection = conn;
                    this._connection.ConnectionShutdown += ConnectionShutdown;
                }
            }

            return this._connection;
        }

        private async void ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            // [VZ:REVISIT] Shall we dispose of old internal this._connection here? Currently it is disposed right before new connection is created.
            await Task.Delay(100);
            this.OnShutdownConnection(new EventArgs());
        }

        private async Task<IConnection> GetConnectionAsync_WaitAndRetry()
        {
            IConnection connection = null;

            int retryCount = 3;
            var policy = RetryPolicy.Handle<BrokerUnreachableException>()
                .Or<System.Net.Sockets.SocketException>()
                .WaitAndRetryAsync(
                    retryCount, 
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (ex, time) =>
                        {
                            string msg = String.Format("Could not connect to RabbitMQ broker after {0}s (Exception: {1})", $"{time.TotalSeconds:n1}", ex.Message);
                            this._logger.LogWarning(msg);
                        }
                    );

                connection = await policy.ExecuteAsync(async () => await Task.Run(() => this.CreateConnection(this._config)));
                // [VZ:] The later could be encapsulated into the method CreateConnectionAsync() and called as:
                //connection = await policy.ExecuteAsync<IConnection>(() => this.CreateConnectionAsync());
                //connection = await policy.ExecuteAsync(this.CreateConnectionAsync);

            return connection;
        }

        private async Task<IConnection> GetConnectionAsync_WaitAndRetryForever()
        {
            IConnection connection = null;

            int retryCount = 0;
            var policy = RetryPolicy.Handle<BrokerUnreachableException>()
                .Or<System.Net.Sockets.SocketException>()
                .WaitAndRetryForeverAsync(
                    sleepDurationProvider: attempt => TimeSpan.FromSeconds(3), //  TimeSpan.FromMinutes(2), // wait 2 min between each try.
                    onRetry: (exception, calculatedWaitDuration) => // capture some info for logging
            {
                // This is basically your new exception handler:
                string msg = String.Format("Exception trying to connect to RabbitMQ Brocker. Number of attempts: {0}. Message: {1}", retryCount + 1, exception.Message);
                this._logger.LogError(msg);
                retryCount++;
            });

            connection = await policy.ExecuteAsync(async () => await Task.Run(() => this.CreateConnection(this._config)));
            
            return connection;
        }

        public bool IsConnected => (
            this._connection != null && 
            this._connection.IsOpen && 
            !this._isDisposed
            );

        public IModel CreateModel()
        {
            IModel model = null;

            if (this.IsConnected)
            {
                model = this._connection.CreateModel();
            }

            return model;
        }

        protected virtual void OnShutdownConnection(EventArgs args)
        {
            this.ShutdownConnection?.Invoke(this, args);
        }


        #region Dispose

        ~RabbitMQResilientConnection()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            if (this._isDisposed) return;

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _isDisposed = true;

            if (disposing && (_connection != null))
            {
                _connection.Dispose();
            }
        }

        #endregion

    }
}
