﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace vDriver.SparkLivy.Interface
{
    public interface ILivyBatchJob
    {
        Task<ILivyBatchJobResult> ExecuteBatchJobAsync(JObject dataObject);
    }
}
