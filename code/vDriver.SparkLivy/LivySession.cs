﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;

using vDriver.Interface;
using vDriver.Library;


namespace vDriver.SparkLivy
{
    public class LivySession : IDisposable
    {
        private readonly ILogger _logger;
        private readonly HttpClient _client;
        private readonly LivySessionConfig _config;
        private readonly string _sessionPath;

        public LivySession(ILogger logger, HttpClient client, LivySessionConfig config, string sessionPath)
        {
            this._logger = logger;
            this._client = client;
            this._config = config;
            this._sessionPath = sessionPath;
        }

        //public async Task<LivySessionState> GetSessionStateAsync(string sessionPath)
        //{
        //    var jObject = await GetResultAsync(sessionPath).ConfigureAwait(false);

        //    var state = GetSessionState(jObject["state"].ToString());

        //    return state;
        //}

        public Task CloseAsync()
        {
            this._logger.LogError("Missing LivySessionConfig in LivySession object. Closing Livy session...");

            return _client.DeleteAsync(_sessionPath);
        }

        public async Task<string> ExecuteStatementAsync(string code)
        {
            var requestDataObject = new JObject
            {
                { "code", code }
            };

            return await this.ExecuteStatementAsync(requestDataObject);
        }

        public async Task<string> ExecuteStatementAsync(string code, SparkCodeKind kind)
        {
            var requestDataObject = new JObject
            {
                { "code", code },
                { "kind", LivySparkUtils.TranslateCodeKind(kind) }
            };

            return await this.ExecuteStatementAsync(requestDataObject);
        }

        private async Task<string> ExecuteStatementAsync(JObject requestDataObject)
        {
            string rslt = string.Empty;

            bool sessionReady = await WaitForSessionAsync().ConfigureAwait(false);
            if (sessionReady)
            {
                // [VZ:HOTSPOT] script is submitted for execution here:
                var response = await _client.PostAsync($"{_sessionPath}/statements", requestDataObject)
                                        .ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var resultPollingRelativePath = response.Headers.Location.AsRelativePath();

                    // [VZ:TODO] Come up with a better name: this is not a session state, this is a state of statement execution 
                    var result = await WaitForSessionStateAsync(resultPollingRelativePath, LivySessionState.Available)
                        .ConfigureAwait(false);

                    var output = result["output"];
                    if (output["status"].ToString() == "error")
                    {
                        rslt = "Error executing statement";
                    }
                    else
                    {
                        var data = output["data"]; 
                        // RTT: data ==> Newtonsoft.Json.Linq.JObject
                        // RTT: data["text/plain"] ==> Newtonsoft.Json.Linq.JValue

                        rslt = output["data"]["text/plain"].ToString();
                    }
                }
                else
                {
                    rslt = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
            }
            else
            {
                rslt = "Cannot establish Spark session";
            }

            return rslt;
        }


        public async Task<bool> WaitForSessionAsync()
        {
            var result = await WaitForSessionStateAsync(_sessionPath, 
                LivySessionState.Idle, 
                LivySessionState.Error, 
                LivySessionState.Dead, 
                LivySessionState.ShuttingDown)
                .ConfigureAwait(false);

            if (result["state"].ToString() == "idle")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Wait for execution state of submitted statement
        /// </summary>
        /// <param name="pollingUri"></param>
        /// <param name="expectedStates"></param>
        /// <returns></returns>
        private async Task<JObject> WaitForSessionStateAsync(string pollingUri, params LivySessionState[] expectedStates)
        {
            for (var attempt = 0; ; attempt++)
            {
                var jObject = await GetResultAsync(pollingUri).ConfigureAwait(false);

                LivySessionState state = this.ParseSessionState(jObject["state"].ToString());
                if (expectedStates.Contains(state))
                {
                    return jObject;
                }

                if (attempt == 300)
                {
                    // [VZ:TODO]
                    //Log($"Failed to get the session into desired state {expectedStates.AsStrings().Join(", ")} after 30 seconds, current status: {state}");

                    attempt = 0;
                }

                // TODO: Decide a reasonable configurable delay
                await Task.Delay(100).ConfigureAwait(false);
            }
        }


        private async Task<JObject> GetResultAsync(string uri)
        {
            var response = await Policy.Handle<TaskCanceledException>()
                                       .RetryAsync(3)
                                       .ExecuteAsync(() => _client.GetAsync(uri))
                                       .ConfigureAwait(false);

            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return JObject.Parse(result);
        }


        // [VZ:] Repacing this with the one below to get rid of Exception:
        //private static LivySessionState GetSessionState(string stateString) =>
        //    Enum.TryParse<LivySessionState>(stateString.Replace("_", ""), true, out var state) ?
        //    state :
        //    throw new Exception($"Unknown state {stateString}");

        // [VZ:REVISIT] Why we need stateString.Replace("_", "") (see above). Getting rid of it:
        private LivySessionState ParseSessionState(string stateString) =>
          Enum.TryParse<LivySessionState>(stateString, true, out var state) ? state : LivySessionState.Unknown;


        #region Dispose

        ~LivySession()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    CloseAsync().GetAwaiter().GetResult();
                }
                catch { }
            }
        }

        #endregion

    }
}
