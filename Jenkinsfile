#!groovy
@Library('pipeline-library')
import opus.jenkins.tasks.*

def pipelineSteps = null;
def teamRoomName = 'Alacra-Builds';
def buildRoomName = 'Alacra-Builds';
def mailRecipients = 'vasyl.zayachkivsky@opus.com';

pipeline {
  agent { label 'alacra' }
  parameters {
    booleanParam(defaultValue: false, description: 'Publish NuGet packages to Nexus (nexus.hiperos-devops.com).', name: 'Publish')
    choice(choices: '2015\n2013\n2017', description: 'Choose the year of the Visual Studio Version you would like to build with.', name: 'VSVersion')
  }
  stages {
    stage('init') {
      steps {
        script {
          pipelineSteps = new DotNetFramework(this)
        }
      }
    }
    stage('clone-build-tools') {
      steps {
        script {
          try {
            pipelineSteps.getBuildBootStrapResources()  
          } catch (e) {
            pipelineSteps.notifyFailed(teamRoomName, null, e.getMessage())
            echo 'The clone-build-tools stage has failed.'
            throw e          
          }
        }
      }
    }
    stage('restore-dependencies') {
      steps { 
        script {
          try {
            pipelineSteps.nugetRestore(env.WORKSPACE + "\\code\\vDriver.Runtime.sln")            
          } catch (e) {
            pipelineSteps.notifyFailed(teamRoomName, null, e.getMessage())
            echo 'The restore-dependencies stage has failed.'
            throw e           
          }
        }
      }
    }
    stage('make') {
      steps {
        script {
          try {
            pipelineSteps.generateSimpleVersion("version.file")
            pipelineSteps.tagSimpleVersion()
            pipelineSteps.buildDotNetSLN(env.WORKSPACE + "\\code\\vDriver.Runtime.sln", "Clean,Build", "Release", "")            
          } catch (e) {
            pipelineSteps.notifyFailed(teamRoomName, null, e.getMessage())
            echo 'The make stage has failed.'
            throw e           
          }
        }
      }
    }
    stage('package') {
      steps {
        script {
          try {
            pipelineSteps.nugetPack(env.WORKSPACE + "\\pm\\Package.nuspec","-suffix ${pipelineVars.Tag}")
          } catch (e) {
            pipelineSteps.notifyFailed(teamRoomName, null, e.getMessage())
            echo 'The package stage has failed.'
            throw e           
          }
        }
      }
    }
    stage('publish') {
      steps {
        script {
          try {
            pipelineSteps.nugetPush('alacra')
          } catch (e) {
            pipelineSteps.notifyFailed(teamRoomName, null, "Build on ${env.BRANCH_NAME} has failed.")
            echo 'The publish step has failed.'
            throw e
          }
        pipelineSteps.notifySuccess(teamRoomName)
        }
      }
    }
  }
}