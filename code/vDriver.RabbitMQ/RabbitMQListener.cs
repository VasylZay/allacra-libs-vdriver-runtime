﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using vDriver.Interface;
using vDriver.RabbitMQ.Interface;

namespace vDriver.RabbitMQ
{
    public class RabbitMQListener : IRabbitMQListener, IDisposable
    {
        private readonly IRabbitMQResilientConnection _connection;
        private readonly string _queueName;

        private IModel _channel;
        private EventingBasicConsumer _consumer;
        private object _lockConsumer = new object();

        private Func<BasicDeliverEventArgs, Task> _doWorkCallback;
        private Action<string> _cancelWorkCallback;
        private volatile bool _isWorkCancelled;


        public RabbitMQListener(ILogger logger, IRabbitMQResilientConnection connection, string queueName)
        {
            this._connection = connection;
            this._queueName = queueName;

            this.Initialize();
        }

        public ILogger Logger { get; private set; }

        private void Initialize()
        {
            IModel channel = this._connection.CreateModel();
            channel.ModelShutdown += Channel_ModelShutdown;

            channel.QueueDeclare(
                queue: _queueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += MessageReceived;

            lock (_lockConsumer)
            {
                if (this._consumer != null)
                {
                    this._consumer.Received -= MessageReceived;
                }

                if (this._channel != null)
                {
                    this._channel.Dispose();
                }

                this._channel = channel;
                this._consumer = new EventingBasicConsumer(channel);
                this._consumer.Received += MessageReceived;
            }
        }


        public void RegisterCallbacks(Func<BasicDeliverEventArgs, Task> doWorkCallback, Action<string> cancelWorkCallback)
        {
            this._doWorkCallback = doWorkCallback;
            this._cancelWorkCallback = cancelWorkCallback;
        }

        private async void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            if (args.Body != null)
            {
                try
                {
                    if (this._doWorkCallback != null)
                    {
                        await this._doWorkCallback(args);
                    }
                }
                catch (Exception e)
                {
                    // [VZ:] Most likely it would be the following Exception caused by network latency:
                    //System.IO.IOException
                    //HResult = 0x80131620
                    //Message = Unable to write data to the transport connection: An existing connection was forcibly closed by the remote host.

                    this.Logger.LogError(e, "Exception in RabbitMQListener.MessageReceived(..) method.");

                    this._isWorkCancelled = true;
                    if (_cancelWorkCallback != null)
                    {
                        this._cancelWorkCallback(e.Message);
                    }
                }
            }
        }

        public void Start()
        {
            if (this._channel != null)
            {
                this._channel.BasicConsume(
                    queue: _queueName,
                    autoAck: false,
                    consumer: this._consumer
                    );
            }
        }


        public void AcknowledgeMessageDelivery(ulong deliveryTag)
        {
            if (this._channel.IsOpen)
            {
                this._channel.BasicAck(deliveryTag: deliveryTag, multiple: false);

                //this._channel.BasicNack(deliveryTag: deliveryTag, multiple: false, requeue: true);
            }
        }


        private void Channel_ModelShutdown(object sender, ShutdownEventArgs e)
        {
            if (!this._isWorkCancelled)
            {
                this._isWorkCancelled = true;
                this._cancelWorkCallback?.Invoke(e.ReplyText);
            }
        }

        #region Dispose

        ~RabbitMQListener()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._consumer != null)
                {
                    this._consumer.Received -= MessageReceived;
                }

                if (this._channel != null)
                {
                    this._channel.Dispose();
                }
            }
        }

        #endregion
    }
}
