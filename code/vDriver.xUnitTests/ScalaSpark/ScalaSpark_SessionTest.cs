﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;
using vDriver.LivySpark;


namespace vDriver.IntegrationTests
{
    public class ScalaSpark_SessionTest
    {
        [Fact]
        [Trait("Category", "Integration")]
        public async Task Test()
        {
            // Arrange ---------------------------------------------
            LivySessionConfig config = new LivySessionConfig
            {
                Url = vDriverConfig.GetLivySparkClusterUrl()
            };
            // -----------------------------------------------------

            // Act -------------------------------------------------
            LivySession session = await LivySparkSessionConnector.CreateSessionAsync(config);
            Assert.NotNull(session);

            bool sessionReady = await session.WaitForSessionAsync().ConfigureAwait(false);
            session.Dispose();
            // -----------------------------------------------------

            // Assert ----------------------------------------------
            Assert.True(sessionReady);
            // -----------------------------------------------------
        }
    }
}
