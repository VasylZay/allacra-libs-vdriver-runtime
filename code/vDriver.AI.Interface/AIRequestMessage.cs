﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.AI.Interface
{
    public class AIRequestMessage
    {
        public Guid message_id { get; set; }
        public DateTime timestamp { get; set; }
        public RequestRollupBatch request_rollup_batch { get; set; }
    }

    public class RequestRollupBatch
    {
        public Guid id { get; set; }
        public string service_id { get; set; }
        public List<Document> documents { get; set; }
    }

    public class Document
    {
        public string id { get; set; }
        public uint historic { get; set; }
    }

}
