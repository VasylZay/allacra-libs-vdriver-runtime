﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vDriver.Interface
{
    public interface IRabbitMQConfiguration
    {
        ushort Port { get; }
        string ServerHost { get; }
        string UserName { get; }
        string Password { get; }
    }
}
