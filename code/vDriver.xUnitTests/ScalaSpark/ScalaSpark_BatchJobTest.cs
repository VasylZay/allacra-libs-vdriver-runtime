﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

using Xunit;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using vDriver.Interface;
using vDriver.Runtime;
using vDriver.SparkLivy.Interface;


namespace vDriver.IntegrationTests
{
    public class ScalaSpark_BatchJobTest : IClassFixture<vDriverFixture>
    {
        private readonly vDriverApp _app;

        public ScalaSpark_BatchJobTest(vDriverFixture fixture)
        {
            this._app = fixture.App;
            this.SparkLivyService = this._app.GetServiceProvider().GetService< ISparkLivyService>();
        }

        // [VZ:REVISIT] Dependency Injection here works erratically. Shall we have xUnit for .NET Core instead for .NET Framework?
        //public ScalaSpark_BatchJobTest(vDriverFixture fixture, ISparkLivyService service)
        //{
        //    this._app = fixture.App;
        //    this.SparkLivyService = service;
        //}

        public ISparkLivyService SparkLivyService { get; private set; }

        [Fact]
        [Trait("Category", "Integration")]
        public async Task Test()
        {
            // Arrange ---------------------------------------------
            ILivyBatchJob job = this.SparkLivyService.CreateSparkLivyBacthJob();
            var dataObject = this.ConfigureClassifierApp();
            // -----------------------------------------------------

            // Act -------------------------------------------------
            ILivyBatchJobResult result = await job.ExecuteBatchJobAsync(dataObject);
            // -----------------------------------------------------


            // Assert ----------------------------------------------
            if (result == null)
            {
                string message = "Livy Spark batch job failed.";
                System.Diagnostics.Debug.WriteLine(message);

                throw new Exception(message);
            }
            else if (result.SuccessFlag)
            {
                string message = result.Message + Environment.NewLine + JsonConvert.SerializeObject(result.Result);
                System.Diagnostics.Debug.WriteLine(message);
            }
            else
            {
                string message = "Livy Spark batch job failed." + Environment.NewLine + result.Message;
                System.Diagnostics.Debug.WriteLine(message);

                throw new Exception(message);
            }
            // -----------------------------------------------------
        }


        private JObject ConfigureClassifierApp()
        {
            var dataObject = new JObject();

            #region ClassifierApp
            string batchJobName = DateTime.Now.ToString("yyyy-MM-dd-T-HH-mm-ss");

            dataObject.Add("file", "s3://vz-bucket/jars/classifier_2.11-1.0.jar");
            dataObject.Add("className", "com.alacra.spark.ClassifierApp");

            // [VZ:REVISIT] Looks like the following has no effect:
            dataObject.Add("name", "Vasyl's name of this batch job");

            string modelPath = "s3://factiva-tprm/model_1.0";
            string storySource = "s3://factiva-tprm/20170910.txt.gz";
            string resultsPath = String.Format("s3://vasylz-us-east-1/results/{0}", batchJobName);

            var argsObject = new JArray(new string[] { modelPath, storySource, resultsPath });

            dataObject.Add("args", argsObject);

            #endregion

            return dataObject;
        }

    }
}
