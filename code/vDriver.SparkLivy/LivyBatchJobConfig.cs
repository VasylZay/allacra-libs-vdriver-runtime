﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.SparkLivy
{
    public class LivyBatchJobConfig
    {
        public string Url { get; set; }
        public int LivyBatchJobPollingDelay { get; set; }
    }
}
