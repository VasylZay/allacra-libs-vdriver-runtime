﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RabbitMQ.Client;

namespace vDriver.RabbitMQ.Interface
{
    public interface IRabbitMQResilientConnection
    {
        event EventHandler ShutdownConnection;

        Task InitializeAsync();
        bool IsConnected { get; }
        IModel CreateModel();
    }
}
