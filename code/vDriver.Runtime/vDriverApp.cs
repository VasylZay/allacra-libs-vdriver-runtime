﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using vDriver.Interface;
using vDriver.Library;

using vDriver.AI.Interface;
using vDriver.AI;

using vDriver.AmazonS3.Interface;
using vDriver.AmazonS3;

using vDriver.DocumentLocker;
using vDriver.DocumentLocker.Interface;

using vDriver.RabbitMQ.Interface;
using vDriver.RabbitMQ;

using vDriver.SparkLivy;
using vDriver.SparkLivy.Interface;

// [VZ:]
//Bitbucket Repository:
//allacra-libs-vdriver-runtime/
//https://VasylZay@bitbucket.org/VasylZay/allacra-libs-vdriver-runtime.git

namespace vDriver.Runtime
{
    // [VZ:TODO] Implement IDisposable. Theoretically it is needed here to dispose ServiceProvider, practically - not.
    public class vDriverApp : IAppService
    {
        public event EventHandler ShutdownApplication;

        private IServiceProvider ServiceProvider { get; set; }
        public ILogger Logger { get; private set; }
        private vDriverConfig vDriverConfig { get; set; }

        public vDriverApp()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception) args.ExceptionObject;

            if (this.Logger != null)
            {
                this.Logger.LogCritical("Unhandled Exception: " + e.Message);
            }
        }

        public void Initialize()
        {
            IServiceCollection services = new ServiceCollection();

            // [1] deal with Logger first:
            this.ConfigureLoginService(services);
            this.ServiceProvider = services.BuildServiceProvider();

            ILoggerFactory loggerFactory = ServiceProvider.GetService<ILoggerFactory>();
            AppLogging.LoggerFactory = loggerFactory;
            this.Logger = loggerFactory.CreateLogger<vDriverApp>();

            IConfigurationRoot configuration = this.GetConfiguration();
            services.AddSingleton<IConfigurationRoot>(configuration);

            // Support typed Options
            services.AddOptions();
            services.Configure<AIServiceConfig>(configuration.GetSection("AIServiceConfig"));
            services.Configure<AmazonS3Config>(configuration.GetSection("AmazonS3Config"));
            services.Configure<DocumentLockerConfig>(configuration.GetSection("DocumentLockerConfig"));
            services.Configure<RabbitMQConfig>(configuration.GetSection("RabbitMQConfig"));
            services.Configure<SparkLivyConfig>(configuration.GetSection("SparkLivyConfig"));
            services.Configure<vDriver.Tests.TestOptions>(configuration.GetSection("vDriver.Tests.TestOptions"));

            this.vDriverConfig = new vDriverConfig();

            // [2] deal with the rest services:
            this.ConfigureServices(services);
            this.ServiceProvider = services.BuildServiceProvider();
        }

        private IConfigurationRoot GetConfiguration()
        {
            return new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("vDriver.AIService.config.json", optional: true)
                .AddJsonFile("vDriver.AmazonS3.config.json", optional: true)
                .AddJsonFile("vDriver.DocumentLocker.config.json", optional: true)
                .AddJsonFile("vDriver.RabbitMQ.config.json", optional: true)
                .AddJsonFile("vDriver.SparkLivy.config.json", optional: true)
                .AddJsonFile("test-appsettings.json", optional: true)
                //.AddJsonFile("test-appsettings.json")
                .Build();
        }


        public async Task Start()
        {
            try
            {
                // [VZ:TODO] This shoud go to XUnit testes:
                #region Scaffolded tests:

                if (IsInteractiveMode)
                {
                    // [1]
                    // [1.1]
                    //await this.SendTestMessage();
                    // [1.2]
                    //await this.SendCannedJobs();
                    //
                    // [1.3]
                    int messageCount = 100;
                    TimeSpan jobTime = new TimeSpan(0, 0, 1); // 1 sec;
                    await this.RunAIPerformanceTest(messageCount, jobTime);


                    // [2]
                    //var s3Test = ServiceProvider.GetService<IAmazonS3Test>();
                    //await s3Test.Run();

                    // [3]
                    //var lockerTest = ServiceProvider.GetService<IDocumentLockerTest>();
                    //await lockerTest.Run();

                    // [4]
                    //await this.TestCallLambda();
                    //await Task.Delay(20000);
                    //await this.TestCallLambda();
                }

                #endregion

                //var serviceAI = ServiceProvider.GetService<IAIService>();
                //await serviceAI.Run();
            }
            catch (Exception ex)
            {
                if (this.Logger != null)
                {
                    this.Logger.LogCritical("Unhandled Exception: " + ex.Message);
                }
            }
        }


        public void ConfigureLoginService(IServiceCollection services)
        {
            services.AddLogging(opt =>
            {
                opt.AddConsole();
                opt.AddDebug();
            });
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IAppService>(this);
            services.AddSingleton<IAppConfig>(this.vDriverConfig.GetAppConfig());

            services.AddSingleton<IAmazonS3Service, AmazonS3Service>();
            services.AddSingleton<IDocumentLockerService, DocumentLockerService>();
            services.AddSingleton<IRabbitMQService, RabbitMQService>();
            services.AddSingleton<ISparkLivyService, SparkLivyService>();

            services.AddSingleton<IAIService, AIService>();

            // [VZ:CLEANUP] This will go away:
            // [VZ:DEMO]
            #region Tests 

            services.AddTransient<IRabbitMQTest, Tests.RabbitMQTest>();
            services.AddTransient<IAmazonS3Test, Tests.AmazonS3Test>();
            services.AddTransient<IDocumentLockerTest, Tests.DocumentLockerTest>();

            // [VZ:] The following is only used for performance testing to see how much overhead vDriver adds. Should be below 5 ms per RabbitMQ message.
            services.AddSingleton<IAIPerformanceTestService, AIPerformanceTestService>();

            #endregion
        }

        public IServiceProvider GetServiceProvider()
        {
            return this.ServiceProvider;
        }


        public bool IsInteractiveMode { get; set; } = false;


        public IAppConfig GetAppConfig()
        {
            return vDriverConfig.GetAppConfig();
        }


        public void ShutDownApplication()
        {
            this.Logger.LogInformation("Exiting application...");

            System.Threading.Thread.Sleep(1000);
            this.OnShutdown(new EventArgs());

            Environment.Exit(1);
        }

        protected virtual void OnShutdown(EventArgs args)
        {
            this.ShutdownApplication?.Invoke(this, args);
        }

        // [VZ:] All these tests below will eventually become XUnit integration tests:
        #region Tests

        private async Task SendTestMessage()
        {
            var mqTest = ServiceProvider.GetService<IRabbitMQTest>();
            await mqTest.SendTestMessage();
        }

        private async Task SendCannedJobs()
        {
            var mqTest = ServiceProvider.GetService<IRabbitMQTest>();
            await mqTest.SendCannedJobs();
        }

        private async Task RunAIPerformanceTest(int messageCount, TimeSpan jobTime)
        {
            var test = new Tests.AI.AIPerformanceTest(this.ServiceProvider);
            await test.Run(messageCount, jobTime);
        }

        private async Task TestCallLambda()
        {

            var lockerService = ServiceProvider.GetService<IDocumentLockerService>();

            Stopwatch stopwatch = Stopwatch.StartNew();
            string payload = await lockerService.CallLambdaExperimental();

            stopwatch.Stop();
            string msg = string.Format("Time spent: {0} milliseonds", stopwatch.ElapsedMilliseconds);
            Logger.LogInformation(msg);
        }

        #endregion
    }
}