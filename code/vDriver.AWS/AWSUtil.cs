﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;


namespace vDriver.AWS
{
    public static class AWSUtil
    {
        public static RegionEndpoint GetRegionEndpoint(string region)
        {
            RegionEndpoint endpoint = null;
            switch (region)
            {
                // Note: You could add more valid AWS Regions as needed.
                case ("us-east-1"):
                    endpoint = RegionEndpoint.USEast1;
                    break;

                case ("us-east-2"):
                    endpoint = RegionEndpoint.USEast2;
                    break;

            }

            return endpoint;
        }

        public static AWSCredentials GetAWSCredentialsUsingProfileName(string profileName)
        {
            CredentialProfileStoreChain chain = new CredentialProfileStoreChain();
            if (chain.TryGetAWSCredentials(profileName, out AWSCredentials awsCredentials))
            {
            }

            return awsCredentials;
        }

    }
}
