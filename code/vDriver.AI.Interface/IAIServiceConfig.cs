﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.AI.Interface
{
    public interface IAIServiceConfig
    {
        string AIJobsFolderPath { get; }
        string AIS3BaseBucket { get; }
        string AIS3WorkingFolder { get; }
        int AIWorkersCount { get; }
        string AI_RabbitMQ_QueueName { get; }
    }
}
