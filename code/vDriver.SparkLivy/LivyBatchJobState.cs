﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vDriver.SparkLivy
{
    // [VZ:REVISIT] The enumeration of the possible values of the state field of the Batch "REST object" 
    // is not specified in the official documentation:
    // https://livy.incubator.apache.org/docs/latest/rest-api.html#batch
    // Also see: 
    // https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.5/bk_spark-component-guide/content/livy-api-batch.html
    // GET /batches/{batchId}/state returns the state of batch session:
    // id       - a non-negative integer that represents a specific batch session int
    // state    - the current state of batch session  string

    public enum LivyBatchJobState
    {
        Unknown = 0,
        Starting,
        Running,
        Success,
        Error
    }
}
