﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

// [VZ:REVISIT] Remove from References too!
using System.Net;

using vDriver.Interface;


namespace vDriver.DocumentLocker
{
    // [VZ:] This is to simmulate communication with Document Locker using EAP = Event-based Asynchronous Pattern 
    public class DocumentLockerWorkerExperimental
    {
        // THE SONNETS by William Shakespeare - 5.19 MB
        // https://raw.githubusercontent.com/hortonworks/data-tutorials/master/tutorials/hdp/setting-up-a-spark-development-environment-with-scala/assets/shakespeare.txt

        public DocumentLockerWorkerExperimental(IAppService appService, ILogger logger)
        {
            this.AppService = appService;
            this.Logger = logger;
        }

        public IAppService AppService { get; private set; }
        public ILogger Logger { get; private set; }


        public async Task<Tuple<bool, string>> RequestDocumentsAsync(string s3CommandLocation, string destinationLocation)
        {
            // [VZ:DEMO]
            //
            // [1] EAP (Event-based Asynchronous Pattern) using polling
            return await Task.Run<Tuple<bool, string>>(() => this.RequestDocumentsUsingPolling(s3CommandLocation, destinationLocation)).ConfigureAwait(false);
            //
            // [2] EAP (Event-based Asynchronous Pattern) using TaskCompletionSource
            //return await Task.Run<Tuple<bool, string>>(() => RequestDocumentsUsingTaskCompletionSource(s3CommandLocation, destinationLocation)).ConfigureAwait(false);
        }

        public Tuple<bool, string> RequestDocumentsUsingTaskCompletionSource(string s3CommandLocation, string destinationLocation)
        {
            // [VZ:CLEANUP]
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string msg = string.Format("DocumentLocker worker is running on the managed thread, id = {0}", hndl);
            this.Logger.LogInformation(msg);

            bool successFlag = false;
            string rslt = string.Empty;

            DocumentLockerClientTaskCompletionSourceBased lockerClient = new DocumentLockerClientTaskCompletionSourceBased();
            TaskCompletionSource<string> tcs = lockerClient.RequestDocumentsAsync(s3CommandLocation, destinationLocation);

            try
            {
                if(tcs.Task.IsFaulted)
                {
                    rslt = tcs.Task.Exception.Message;
                }
                else
                {
                    string filePath = this.WriteResults(tcs.Task.Result);
                    if(filePath != null)
                    {
                        successFlag = true;
                        rslt = filePath;
                    }
                    else
                    {
                        rslt = "Failed to request documents in the DocumentLockerWorker." ;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "Failed to request documents in the DocumentLockerWorkerExperimental.");
            }

            return new Tuple<bool, string>(successFlag, rslt);
        }

        #region Polling 

        private bool _downloadCompleted;

        public async Task<Tuple<bool, string>> RequestDocumentsUsingPolling(string s3CommandLocation, string destinationLocation)
        {
            // [VZ:CLEANUP]
            int hndl = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string msg = string.Format("DocumentLocker worker is running on the managed thread, id = {0}", hndl);
            this.Logger.LogInformation(msg);


            bool successFlag = false;
            string rslt = string.Empty;

            using (DocumentLockerClientEAPPolling lockerClient = new DocumentLockerClientEAPPolling())
            {
                lockerClient.RequestCompleted += LockerClient_RequestCompleted;
                lockerClient.RequestDocumentsAsync(s3CommandLocation, destinationLocation);
                do
                {
                    await Task.Delay(100);
                }
                while (!this._downloadCompleted);

                successFlag = lockerClient.SuccessFlag;
                if (successFlag)
                {
                    string filePath = this.WriteResults(lockerClient.Result);
                    if (filePath != null)
                    {
                        successFlag = true;
                        rslt = filePath;
                    }
                    else
                    {
                        rslt = "Failed to request documents in the DocumentLockerWorker.";
                    }
                }
                else
                {
                    rslt = lockerClient.ErrorMessage;
                }
            }

            return new Tuple<bool, string>(successFlag, rslt);
        }


        private void LockerClient_RequestCompleted(object sender, EventArgs e)
        {
            this._downloadCompleted = true;
        }

        #endregion

        private string WriteResults(string payload)
        {
            string filePath = null;

            try
            {
                string dirPath = this.AppService.GetAppConfig().TempDirectory;

                filePath = Path.Combine(dirPath, Path.GetRandomFileName());
                filePath = filePath + ".txt";
                File.WriteAllText(filePath, payload);
            }
            catch (Exception ex)
            {
                this.Logger.LogError(ex, "Failed to write to the temp directory.");
            }

            return filePath;
        }
    }

}
